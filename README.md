# README #

Group project as part of the media informatics course at the "Hochschule der Medien" (Media University).

### About the game ###

VoronoiWars is a turnbased strategy game on procedurale generated maps for two players. It contains HotSeat and LAN modes.

### About the implementation ###

It's created with Unity3D and is written in C#. 