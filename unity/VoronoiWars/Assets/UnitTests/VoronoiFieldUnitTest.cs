﻿using UnityEngine;
using System.Collections.Generic;

/*
 * Author: Jakob Schaal
 */
public class VoronoiFieldUnitTest : MonoBehaviour {

	// Use this for initialization
	void Start () {
		Debug.Log ("#####BEGIN VORONOIFIELDTEST#####");
		//a voronoifield of 3 vertices should ALWAYS be valid
		/*for (int i = 0; i<10000; i++) {
			List<Vector2Double> l = new List<Vector2Double>();
			l.Add (Random.insideUnitCircle * 1000);
			l.Add (Random.insideUnitCircle * 1000);
			l.Add (Random.insideUnitCircle * 1000);
			VoronoiField vf = new VoronoiField(l);
		}*/
		Debug.Log ("VoronoiFieldUnitTest: Random Vetices Test sucessful!");


		List<Vector2Double> li = new List<Vector2Double>();
		li.Add (new Vector2Double (0, 0));
		li.Add (new Vector2Double (-3, 0));
		li.Add (new Vector2Double (-3, -3));
		li.Add (new Vector2Double (0, -3));
		VoronoiField vf2 = new VoronoiField(li);
		Debug.Log ("VoronoiFieldUnitTest: Good Field Test sucessful!");


		if (vf2.calculateArea () == 9) {
			Debug.Log ("VoronoiFieldUnitTest: Area Test sucessful!");
		} else {
			throw new UnityException("Area is not correct! It was " + vf2.calculateArea());
		}


		Vector2Double centroid = vf2.calculateCentroid ();
		if (centroid.x == -1.5f && centroid.y == -1.5f) {
			Debug.Log ("VoronoiFieldUnitTest: Centroid Test sucessful!");
		} else {
			throw new UnityException("Centroid is not correct! " + centroid.x + " " + centroid.y);
		}


		List<Vector2Double> l3 = new List<Vector2Double> ();
		l3.Add (new Vector2Double(0, 0));
		l3.Add (new Vector2Double(1, 0));
		l3.Add (new Vector2Double(0, 1));
		l3.Add (new Vector2Double(1, 1));
		bool exceptionCaught = false;
		try{
			VoronoiField vf3 = new VoronoiField(l3);
			exceptionCaught = false;
		}catch(UnityException e){
			exceptionCaught = true;
		}
		if (!exceptionCaught) {
			throw new UnityException ("There was a Bad Field Test which didnt throw an Exception (but it should have)!");
		} else {
			Debug.Log ("VoronoiFieldUnitTest: Bad Field Test sucessful!");
		}


		Circle2 c = new Circle2 (new Vector2Double (1, 0), new Vector2Double (2, 1), new Vector2Double (1, 2));
		if (c.center.x != 1 || c.center.y != 1) {
			throw new UnityException("Fehlerhafte Kreisberechnung! " + c.center.x + " " + c.center.y);
		}
		Debug.Log ("Circle Test #1 Done!");


		try{
			c = new Circle2 (new Vector2Double (1, 1), new Vector2Double (2, 2), new Vector2Double (5, 5));
			exceptionCaught = false;
		}catch(UnityException e){
			exceptionCaught = true;
		}

		if (!exceptionCaught) {
			throw new UnityException ("Points for Circle were colinear but there was no exception (but ther should have benn one!)");
		} else {
			Debug.Log ("Circle Test #2 Done!");
		}
		Debug.Log ("#####END VORONOIFIELDTEST SUCESSFUL!#####");
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
