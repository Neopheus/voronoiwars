﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class NextTurnButtonScript : MonoBehaviour {
	public GameObject NTButton;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void AdvanceTurn (){
		//TODO: activate next turn function
		//print ("Next Turn!");
		if (NetworkManager.instance.networkMode == NetworkManager.NetworkMode.hotSeat) {
			GameSupervisor.instance.newRound ();
		} else {
			GameSupervisor.instance.newRoundRPC();
		}
	}	
}
