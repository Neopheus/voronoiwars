﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

//class for managing ressource-depended UI-Elements
public class ResourceButtonScript : MonoBehaviour {
	public GameObject ResText;
	public GameObject AtomBar;
	public GameObject AtomButt;
	private Slider AtomBarSlider;
	private Button AtomButtButton;
	private Text text;

	private Image[] images;
	public Sprite atomDisabled;
	public Sprite atomEnabled;

	private float t;

	// Use this for initialization
	void Start () {
		AtomBarSlider = AtomBar.GetComponent<Slider> ();
		AtomButtButton = AtomButt.GetComponent<Button> ();
		text = ResText.GetComponent<Text> ();
		//Set maximum value for AtomSlider to threshold
		AtomBarSlider.maxValue = Settings.NUKE_COST;
		images = AtomButtButton.GetComponentsInChildren<Image>();
		t = 0;
	}

	// Update is called once per frame
	void Update () {


		//Set resource display
		if (NetworkManager.instance.networkMode == NetworkManager.NetworkMode.hotSeat || GameSupervisor.instance.faction == GameSupervisor.instance.ownFaction) {
			text.text = Settings.CURRENCY + " " + GameSupervisor.instance.resourceCurrent;
			t-=Time.deltaTime;
		} else {
			text.text = "???";
			t+=Time.deltaTime;
		}

		t = Mathf.Clamp01 (t);
		

		//Change AtomSlider according to amount of resources and
		//enable AtomButton only if enough resources are available
		if (GameSupervisor.instance.resourceCurrent >= Settings.NUKE_COST) {
			AtomBarSlider.value = Settings.NUKE_COST;
			AtomButtButton.interactable = true;

			foreach (Image image in images)
			{
				image.sprite = atomEnabled;
			}

		} 
		else {
			AtomBarSlider.value = GameSupervisor.instance.resourceCurrent;
			AtomButtButton.interactable = false;

			foreach (Image image in images)
			{
				image.sprite = atomDisabled;
			}
		}

	}
}
