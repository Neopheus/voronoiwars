using UnityEngine;
using UnityEngine.UI;
using System.Collections;

//manages the radial menu for buying units
public class RadialScript : MonoBehaviour {
	public static RadialScript instance;
	public GameObject RadPanel;
	private bool closed = true;
	private bool clickable = false;
	private bool fadeIn = false;
	private bool fadeOut = false;
	private Vector3 scaleOpen;
	private Vector3 scaleClosed;

	private float timeSinceOpen = 0F;
	private float timeSinceScaleToggle = 0F;
	private float timeToToggle = 0.2F;

	private Button AntiAirButton;
	private Button TankButton;
	private Button HeliButton;


	private void setClickable(bool b){
		clickable = b;
	}

	private void SetTransformX(float n){
		transform.position = new Vector3(n, transform.position.y, transform.position.z);
	}

	// Use this for initialization
	void Start () {
		timeSinceScaleToggle = 0F;
		scaleOpen = RadPanel.transform.localScale;

		if (instance != null)
			throw new UnityException ("Singleton Violation");
		instance = this;

		HeliButton    = this.transform.GetChild(0).GetComponent<Button>();
		TankButton    = this.transform.GetChild(1).GetComponent<Button>();
		AntiAirButton = this.transform.GetChild(2).GetComponent<Button>();
	}
	
	// Update is called once per frame
	void Update () {
		if (closed == false && GameSupervisor.instance.selectedField == null) {
			deactivateRadialMenu();
		}

		if (closed == false) {
			timeSinceOpen += Time.deltaTime;
		}

		clickable = timeSinceOpen > 0.3f;
		CheckButtons ();

		if (fadeIn == true && timeSinceScaleToggle < timeToToggle) {
			timeSinceScaleToggle += Time.deltaTime;
			RadPanel.transform.localScale = scaleOpen * timeSinceScaleToggle/timeToToggle;
			if(timeSinceScaleToggle >= timeToToggle){
				timeSinceScaleToggle = 0;
				fadeIn = false;
			}
		}

		if (fadeOut == true && timeSinceScaleToggle < timeToToggle) {
			timeSinceScaleToggle += Time.deltaTime;
			RadPanel.transform.localScale = scaleOpen - (scaleOpen * timeSinceScaleToggle/timeToToggle);
			if(timeSinceScaleToggle >= timeToToggle){
				timeSinceScaleToggle = 0;
				fadeOut = false;
				RadPanel.transform.localScale = new Vector3(0,0,0);
			}
		}
	}
	

	public void activateRadialMenu(){
		if (!closed) {
			fadeOut = true;
			Debug.Log(" war noch offen");
		}
		fadeIn = true;
		closed = false;
		timeSinceOpen = 0;
		CheckButtons ();
	}

	public void deactivateRadialMenu(){
		clickable = false;
		fadeIn = false; //TODO hier hab ich das angepasst wegen dem gui bug beim schnellen maus rausziehen
		fadeOut = true;
		closed = true;
		GameSupervisor.instance.selectedField = null;
		CheckButtons ();
	}

//	public void fadeIn(){
//		RadPanel.transform.localScale = scaleOpen;
//	}
//
//	public void fadeOut(){
//		RadPanel.transform.localScale = scaleClosed;
//	}

	private void CheckButtons(){
		bool enoughResources = GameSupervisor.instance.resourceCurrent >= Settings.UNIT_COST;

		HeliButton   .interactable = enoughResources;
		TankButton   .interactable = enoughResources;
		AntiAirButton.interactable = enoughResources;
	}

	//buy units in radial menu and deactivate menu afterwards
	public void BuyAir(){
		if (clickable) {
			Camera.main.GetComponent<AudioSource>().Play();
			GameSupervisor.instance.resourceCurrent -= Settings.UNIT_COST;
			if(NetworkManager.instance.networkMode == NetworkManager.NetworkMode.hotSeat)
			{
				GameSupervisor.instance.buyHeli();
			} else {
				GameSupervisor.instance.buyHeliRPC(GameSupervisor.instance.selectedField.id);
			}
			deactivateRadialMenu ();
		}
	}

	public void BuyTank(){
		if (clickable) {
			Camera.main.GetComponent<AudioSource>().Play();
			GameSupervisor.instance.resourceCurrent -= Settings.UNIT_COST;
			if(NetworkManager.instance.networkMode == NetworkManager.NetworkMode.hotSeat)
			{
				GameSupervisor.instance.buyTank();
			} else {
				GameSupervisor.instance.buyTankRPC(GameSupervisor.instance.selectedField.id);
			}
			deactivateRadialMenu ();
		}
	}

	public void BuyAA(){
		if (clickable) {
			Camera.main.GetComponent<AudioSource>().Play();
			GameSupervisor.instance.resourceCurrent -= Settings.UNIT_COST;
			if(NetworkManager.instance.networkMode == NetworkManager.NetworkMode.hotSeat)
			{
				GameSupervisor.instance.buyAntiAir();
			} else {
				GameSupervisor.instance.buyAntiAirRPC(GameSupervisor.instance.selectedField.id);
			}
			deactivateRadialMenu ();
		}
	}
}
