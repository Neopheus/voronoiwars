﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

//manages the radial menu for buying units
public class RadialBackup : MonoBehaviour {
	public static RadialBackup instance;
	public GameObject RadPanel;
	public GameObject RadCanvas;
	public Animator menuAnim;
	private bool closed = true;
	private bool clickable = false;
	
	private float timeSinceOpen = 0;
	
	private Button AntiAirButton;
	private Button TankButton;
	private Button HeliButton;
	
	
	private void setClickable(bool b){
		clickable = b;
	}
	
	private void SetTransformX(float n){
		transform.position = new Vector3(n, transform.position.y, transform.position.z);
	}
	
	// Use this for initialization
	void Start () {
		if (instance != null)
			throw new UnityException ("Singleton Violation");
		instance = this;
		
		HeliButton    = this.transform.GetChild(0).GetComponent<Button>();
		TankButton    = this.transform.GetChild(1).GetComponent<Button>();
		AntiAirButton = this.transform.GetChild(2).GetComponent<Button>();
	}
	
	// Update is called once per frame
	void Update () {
		if (closed == false && GameSupervisor.instance.selectedField == null) {
			deactivateRadialMenu();
		}
		
		if (closed == false) {
			timeSinceOpen += Time.deltaTime;
		}
		
		clickable = timeSinceOpen > 0.3f;
		CheckButtons ();
	}
	
	public void activateRadialMenu(){
		if (!closed) {
			//menuAnim.GetComponent<Animator> ().SetTrigger ("FadeOut");
		}
		//menuAnim.GetComponent<Animator> ().SetTrigger ("FadeIn");
		
		closed = false;
		timeSinceOpen = 0;
		CheckButtons ();
	}
	
	public void deactivateRadialMenu(){
		clickable = false;
		//menuAnim.GetComponent<Animator> ().SetTrigger("FadeOut");
		closed = true;
		GameSupervisor.instance.selectedField = null;
		CheckButtons ();
	}
	
	private void CheckButtons(){
		bool enoughResources = GameSupervisor.instance.resourceCurrent >= Settings.UNIT_COST;
		
		HeliButton   .interactable = enoughResources;
		TankButton   .interactable = enoughResources;
		AntiAirButton.interactable = enoughResources;
	}
	
	//buy units in radial menu and deactivate menu afterwards
	public void BuyAir(){
		if (clickable) {
			GameSupervisor.instance.resourceCurrent -= Settings.UNIT_COST;
			if(NetworkManager.instance.networkMode == NetworkManager.NetworkMode.hotSeat)
			{
				GameSupervisor.instance.buyHeli();
			} else {
				GameSupervisor.instance.buyHeliRPC(GameSupervisor.instance.selectedField.id);
			}
			deactivateRadialMenu ();
		}
	}
	
	public void BuyTank(){
		if (clickable) {
			GameSupervisor.instance.resourceCurrent -= Settings.UNIT_COST;
			if(NetworkManager.instance.networkMode == NetworkManager.NetworkMode.hotSeat)
			{
				GameSupervisor.instance.buyTank();
			} else {
				GameSupervisor.instance.buyTankRPC(GameSupervisor.instance.selectedField.id);
			}
			deactivateRadialMenu ();
		}
	}
	
	public void BuyAA(){
		if (clickable) {
			GameSupervisor.instance.resourceCurrent -= Settings.UNIT_COST;
			if(NetworkManager.instance.networkMode == NetworkManager.NetworkMode.hotSeat)
			{
				GameSupervisor.instance.buyAntiAir();
			} else {
				GameSupervisor.instance.buyAntiAirRPC(GameSupervisor.instance.selectedField.id);
			}
			deactivateRadialMenu ();
		}
	}
}
