﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class InputTestScript : MonoBehaviour {
	public GameObject pos;
	public string stringToEdit = "Hello World";

	private string textField;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		Debug.Log (stringToEdit);
	
	}

	void OnGUI() {
		stringToEdit = GUI.TextField (new Rect (pos.transform.position.x, pos.transform.position.y, 100, 20), stringToEdit, 25);

	}
}
