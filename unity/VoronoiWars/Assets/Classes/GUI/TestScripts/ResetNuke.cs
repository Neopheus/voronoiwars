﻿using UnityEngine;
using System.Collections;

public class ResetNuke : MonoBehaviour {
	public GameObject nuke;
	public Vector3 origPos;

	// Use this for initialization
	void Start () {
		origPos = nuke.transform.position;
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void resetNuke(){
		nuke.transform.position = origPos;
		nuke.transform.LookAt (new Vector3 (origPos.x, origPos.y + 5, origPos.z));
	}
}
