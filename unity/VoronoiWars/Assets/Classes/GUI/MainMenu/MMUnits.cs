﻿using UnityEngine;
using System.Collections;

public class MMUnits : MonoBehaviour {

	public GameObject submarine;
	public GameObject helis;
	private Vector3 rotation_center_sub = new Vector3(-600, 1, 150);
	private Vector3 move_target_sub = new Vector3(56, 1, 1400);
	private float speed_sub = 10F;
	private float step_sub;
	private Vector3 orig_position_sub;

	private Vector3 rotation_center_helis = new Vector3(-600, 1, -600);
	private Vector3 move_target_helis = new Vector3(900, 24, -1700);
	private float speed_helis = -50F;
	private float step_helis;
	private Vector3 orig_position_helis;

	// Use this for initialization
	void Start () {
		orig_position_sub = submarine.transform.position;
		orig_position_helis = helis.transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		step_sub = speed_sub * Time.deltaTime;
		step_helis = speed_helis * Time.deltaTime;
		//submarine.transform.RotateAround(rotation_center_sub, Vector3.up, speed_sub);
		//helis.transform.RotateAround(rotation_center_helis, Vector3.up, speed_helis);
		submarine.transform.position = Vector3.MoveTowards(submarine.transform.position, move_target_sub, step_sub);
		helis.transform.position = Vector3.MoveTowards(helis.transform.position, move_target_helis, step_helis);
		//reset unit positions to loop movement
		if (submarine.transform.position.z >= 1380F) {
			submarine.transform.position = orig_position_sub;
		}
		if (helis.transform.position.z >= 1680F) {
			helis.transform.position = orig_position_helis;
		}
	}
}
