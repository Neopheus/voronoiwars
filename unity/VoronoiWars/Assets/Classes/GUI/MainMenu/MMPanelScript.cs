﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MMPanelScript : MonoBehaviour {
	public static string usedSeed;
	public GameObject MainMenuPanel;
	public GameObject HotSeatPanel;
	public GameObject HostGamePanel;
	public GameObject JoinGamePanel;
	public GameObject CreditsPanel;

	// Use this for initialization
	void Start () {
		HotSeatPanel.SetActive(false);
		HostGamePanel.SetActive(false);
		JoinGamePanel.SetActive(false);
		CreditsPanel.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
