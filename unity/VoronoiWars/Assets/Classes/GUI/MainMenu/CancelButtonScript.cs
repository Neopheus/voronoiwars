﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;


public class CancelButtonScript : MonoBehaviour {
	//Used to switch between the currently active panel and the main menu panel

	public GameObject MainMenuPanel;
	public GameObject OwnPanel;

	public void CancelSelection(){
		MainMenuPanel.SetActive (true);
		OwnPanel.SetActive (false);
	}

	void Update(){
		if (Input.GetKeyDown(KeyCode.Escape)) {
			CancelSelection();
		}
	}
}
