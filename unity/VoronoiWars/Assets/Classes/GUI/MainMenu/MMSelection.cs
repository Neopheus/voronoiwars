﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;


public class MMSelection : MonoBehaviour {
	//Used to switch between main menu panel and the selected panel
	
	public GameObject MainMenuPanel;
	public GameObject TargetPanel;

	public void ActivateSelection(){
		MainMenuPanel.SetActive (false);
		TargetPanel.SetActive (true);
	}
}
