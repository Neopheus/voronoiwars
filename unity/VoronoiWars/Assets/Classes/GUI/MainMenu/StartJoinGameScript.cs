﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class StartJoinGameScript : MonoBehaviour {
	
	public InputField ipInputField;
	public string ip;

	void Start(){
		//only for testing, a default ip
		//ipInputField.text = "192.168.2.117";
		//ipInputField.text = "10.60.58.224";

		ipInputField.text = Network.player.ipAddress;
	}

	void Update(){
		if (Input.GetKeyDown(KeyCode.Return)) {
			Connect ();
		}
		
		if (NetworkManager.instance.tryToConnect) {
			this.GetComponent<Button> ().interactable = false;
		} else {
			this.GetComponent<Button> ().interactable = true;
		}
	}

	public void Connect(){
		ip = ipInputField.text;
		NetworkManager.instance.tryToConnect = true;
		NetworkManager.instance.networkMode = NetworkManager.NetworkMode.isClient;
		NetworkManager.instance.ConnectToServer (ip);
		Debug.Log ("try to connect to " + ip);
	}
}
