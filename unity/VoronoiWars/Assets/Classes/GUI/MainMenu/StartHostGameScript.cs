﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class StartHostGameScript : MonoBehaviour {

	public GameObject SeedInputField;
	public InputField IpField;
	private InputField inp;
	private string seed;

	string ip;
	
	void Start(){
		inp = SeedInputField.GetComponent<InputField>();
		inp.text = Random.Range(1000, 1000000).ToString();
		IpField.GetComponent<InputField>().text = Network.player.ipAddress;
		Debug.Log ("IP: " + Network.player.ipAddress);
	}

	void Update(){
		if (Input.GetKeyDown(KeyCode.Return)) {
			InitializeNetwork();
		}

		if (NetworkManager.instance.serverStarted) {
			this.GetComponent<Button> ().interactable = false;
		} else {
			this.GetComponent<Button> ().interactable = true;
		}
	}

	public void InitializeNetwork(){
		seed = inp.text;
		NetworkManager.instance.serverStarted = true;
		NetworkManager.instance.networkMode = NetworkManager.NetworkMode.isHost;
		NetworkManager.instance.StartServer (seed);
		Debug.Log ("hosted with seed " + seed);
	}
}

