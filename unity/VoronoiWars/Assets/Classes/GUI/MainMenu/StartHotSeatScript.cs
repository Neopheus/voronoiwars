﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class StartHotSeatScript : MonoBehaviour {

	public GameObject SeedInputField;
	private InputField inp;
	public static int Seed = UnityEngine.Random.seed;

	void Start(){
		inp = SeedInputField.GetComponent<InputField>();
		inp.text = Random.Range(1000, 1000000).ToString();
	}

	void Update(){
		if (Input.GetKeyDown(KeyCode.Return)) {
			startHotSeat();
		}
	}

	public void startHotSeat(){
		Seed = inp.text.GetHashCode();
		MMPanelScript.usedSeed = inp.text;
		UnityEngine.Random.seed = Seed;
		Debug.Log ("Starting hot seat with seed: " + Seed);
		NetworkManager.instance.networkMode = NetworkManager.NetworkMode.hotSeat;
		Application.LoadLevel("Dashboard");
	}
}

//public class StartHotSeatScript : MonoBehaviour {
//	
//	public GameObject pos;
//	private string textf;
//	public static int Seed = UnityEngine.Random.seed;
//	
//	void Start(){
//		textf = Random.Range(1000, 1000000).ToString();
//	}
//	
//	void Update(){
//		if (Input.GetKeyDown(KeyCode.Return)) {
//			startHotSeat();
//		}
//	}
//
//	void OnGUI() {
//		textf = GUI.TextField (new Rect (pos.transform.position.x, pos.transform.position.y, 100, 20), textf, 25);	
//	}
//	
//	public void startHotSeat(){
//		Seed = textf.GetHashCode();
//		MMPanelScript.usedSeed = textf;
//		Debug.Log ("Starting hot seat with seed: " + Seed);
//		NetworkManager.instance.networkMode = NetworkManager.NetworkMode.hotSeat;
//		UnityEngine.Random.seed = Seed;
//		Application.LoadLevel("Dashboard");
//	}
//}

//DO NOT DELETE

