﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TurnDisplayScript : MonoBehaviour {
	public Text text;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		text.text = "Turn: " + GameSupervisor.instance.roundNumber;
	}
}
