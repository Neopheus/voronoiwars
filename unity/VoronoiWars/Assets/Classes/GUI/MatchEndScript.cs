﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;


public class MatchEndScript : MonoBehaviour {
	public static MatchEndScript instance;
	public GameObject MatchEndPanel;
	public GameObject closeButton;

	public Sprite backgroundBlue;
	public Sprite backgroundRed;
	private float timeSinceFade = 0f;
	private float timeSinceEnd = 0f;
	public float fadeTime = 7f;
	public float fadeDelay = 3f;

	private Image panelImg;
	private Image buttonImg;
	private bool endMatch = false;
	private bool fade = false;

	public void showMatchEndScreen(Factions fac){
		Debug.Log ("End Match");
		if (fac == Factions.blue) {
			MatchEndPanel.GetComponent<Image>().sprite = backgroundBlue;
			Debug.Log ("Blue wins");
		}
		else{
			MatchEndPanel.GetComponent<Image>().sprite = backgroundRed;
			Debug.Log ("Red wins");
		}

		MatchEndPanel.SetActive (true);
		endMatch = true;
	}


	public void hideMatchEndScreen(){
		MatchEndPanel.SetActive (false);
	}


	// Use this for initialization
	void Start () {
		instance = this;
		hideMatchEndScreen ();

	}

	void Update(){
		if (endMatch && timeSinceEnd <= fadeDelay) {
			timeSinceEnd += Time.deltaTime;
			if(timeSinceEnd >= fadeDelay){
				fade = true;
			}
		}

		if (fade && timeSinceFade <= fadeTime) {
			timeSinceFade += Time.deltaTime;
			panelImg = MatchEndPanel.GetComponent<Image> ();
			buttonImg = closeButton.GetComponent<Image> ();
			Color c = panelImg.color;
			if (c.a < 1f)
				c.a = 1f * timeSinceFade/fadeTime;
			panelImg.color = c;
			buttonImg.color = c;
		}
	}
}
