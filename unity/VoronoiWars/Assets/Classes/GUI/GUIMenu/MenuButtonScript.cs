﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MenuButtonScript : MonoBehaviour {
	public GameObject MenuPanel;
	public GameObject seedText;
	
	private bool menu_open;
	private Vector3 openPos;
	private Vector3 closedPos;

	private float timeToToggle = 0.5f;
	private float timeSinceToggle = 0.0f;

	private float speed = 200F;
	private float step;

	// Use this for initialization
	void Start () {
		openPos = MenuPanel.transform.position;
		closedPos = new Vector3 (openPos.x, openPos.y + 200, openPos.z);
		//MenuPanel.transform.position = closedPos;
		menu_open = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (MMPanelScript.usedSeed != null) {
			seedText.GetComponent<Text> ().text = MMPanelScript.usedSeed;
		}
		else
			step = speed * Time.deltaTime;

		//close menu
		if (!menu_open){
			//MenuPanel.transform.position = Vector3.MoveTowards(MenuPanel.transform.position, closedPos, step);
			MenuPanel.SetActive(false);
		}

		//open menu
		if (menu_open){
			//MenuPanel.transform.position = Vector3.MoveTowards(MenuPanel.transform.position, openPos, step);
			MenuPanel.SetActive(true);
		}
	}

	public void toggleMenu(){
		menu_open = !menu_open;
		Debug.Log ("toggle");
	}
}
