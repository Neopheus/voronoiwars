﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LeaveMatchScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void leaveMatch(){
		PumpJackScript.pumpJacks.Clear ();
		Debug.Log("Leave Match");
		Destroy (NetworkManager.instance);
		Application.LoadLevel ("MainMenuScene");
	}
}
