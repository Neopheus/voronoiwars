﻿using UnityEngine;
using System.Collections;

public class MatchEndClose : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void closeMatch(){
		PumpJackScript.pumpJacks.Clear ();
		Destroy (NetworkManager.instance);
		Debug.Log("Match closed");
		Application.LoadLevel ("MainMenuScene");
	}
}
