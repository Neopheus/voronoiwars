﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HealtBarFrameScript : MonoBehaviour {
	//implements the health bars for units on the battlefield
	public GameObject HealthBar;
	public GameObject HealthBarFrame;
	private GameObject UnitA;
	//TODO getter für health
	private int HealthAmount = 100;
	public Vector3 OffsetVector;
	private float CurrentHealth;
	private float MaxLength_HealthBar;
	private float OriginalY;
	private float OriginalZ;
	
	// Use this for initialization
	void Start () {
		MaxLength_HealthBar = HealthBar.transform.localScale.x;
		OriginalY = HealthBar.transform.localScale.y;
		OriginalZ = HealthBar.transform.localScale.z;

		UnitA = this.transform.parent.parent.gameObject;
	}
	
	// Update is called once per frame
	void Update () {
		//position relative to target unit
		HealthBarFrame.transform.position = UnitA.transform.position + OffsetVector;
		//rotation relative to camera
		HealthBarFrame.transform.rotation = Camera.main.transform.rotation;
		//scale healthbar according to health
		HealthAmount = this.transform.parent.parent.GetComponent<Unit> ().shownHp;
		//print("X: " + MaxLength_HealthBar * (CurrentHealth / MaxHealth));
		HealthBar.transform.localScale = new Vector3 (MaxLength_HealthBar * ((float)this.transform.parent.parent.GetComponent<Unit>().shownHp / this.transform.parent.parent.GetComponent<Unit>().maxHP), 
		                                              OriginalY, 
		                                              OriginalZ);
	}
}
