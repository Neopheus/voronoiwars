﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TimerScript : MonoBehaviour {

	public GameObject TextBox;
	private float timer;
	private Text text;
	
	// Use this for initialization
	void Start () {
		text = TextBox.GetComponent<Text> ();
	}
	
	// Update is called once per frame
	void Update () {
		timer += Time.deltaTime;
	}
	
	void OnGUI() {
		int minutes = Mathf.FloorToInt(timer / 60F);
		int seconds = Mathf.FloorToInt(timer - minutes * 60);
		
		string dispTime = string.Format("{0:00}:{1:00}", minutes, seconds);
		
		text.text = "" + dispTime;
		//print ("" + dispTime);
	}
}
