﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HealtBarScript : MonoBehaviour {
	//implements the health bars for units on the battlefield
	public GameObject HealthBar;
	public GameObject Unit;
	public GameObject CameraToUse;
	//TODO getter für health
	public int HealthAmount = 100;
	private float XOffset = 0;
	private float YOffset = 50f;
	private float ZOffset = 0;
	private Vector3 OffsetVector;
	private float CurrentHealth;
	private float MaxHealth;
	private float MaxLength_HealthBar;
	private float OriginalY;
	private float OriginalZ;

	private float ClampHealth(float h){
		if (h > MaxHealth) {
			h = MaxHealth;
		} else
		if (h < 0) {
			h = 0;
		}
		return h;
	}

	// Use this for initialization
	void Start () {
		OffsetVector = new Vector3 (XOffset, YOffset, ZOffset);
		MaxHealth = (float)HealthAmount;
		MaxLength_HealthBar = HealthBar.transform.localScale.x;
		OriginalY = HealthBar.transform.localScale.y;
		OriginalZ = HealthBar.transform.localScale.z;
	}
	
	// Update is called once per frame
	void Update () {
		//position relative to target unit
		//HealthBar.transform.position = Unit.transform.position + OffsetVector;
		//rotation relative to camera
		HealthBar.transform.rotation = CameraToUse.transform.rotation;
		//scale healthbar according to health
		CurrentHealth = ClampHealth((float)HealthAmount);
		//print("X: " + MaxLength_HealthBar * (CurrentHealth / MaxHealth));
		HealthBar.transform.localScale = new Vector3 (MaxLength_HealthBar * (CurrentHealth / MaxHealth), 
		                                              OriginalY, 
		                                              OriginalZ);
	}
}
