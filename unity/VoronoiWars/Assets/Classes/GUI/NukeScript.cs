﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
//Set interactable by ResourceButtonScript
public class NukeScript : MonoBehaviour {

	public GameObject missile;
	public GameObject nukeExplosion;

	private Vector3 start;
	private Vector3 target;
	private bool missile_moving = false;
	private GameObject missileInstance;

	private float controlPointX;
	private float controlPointY;
	private float controlPointZ;

	private Vector3 nukeOrigPosition;	
	private Factions nukedHq;	
	//the time since starting the movement
	private float passed_time;
	//the amount of time after which a nuke reaches it's target position
	private float time_to_target = 5;
	//ratio of the passed time compared to complete movement time
	private float ratio;


	//Submarine
	public GameObject submarine;
	private bool sub_moving = true;
	private float time_since_emerge = 0;
	private float time_to_emerge = 3;
	private Vector3 rotation_center = new Vector3(100, 1, 100);
	private float speed = 3F;

	public AudioClip nukeStartSound;


	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		if (sub_moving) {
			submarine.transform.RotateAround(rotation_center, Vector3.up, speed * Time.deltaTime);

		} else {
			//submarine emerges and fires nuke
			if(time_since_emerge < time_to_emerge){
				time_since_emerge += Time.deltaTime;
				if(submarine.transform.position.y < 5){
					submarine.transform.position += new Vector3(0, 0.1f * time_since_emerge / time_to_emerge ,0);
				}
			}
			else if(!missile_moving){
				missile_moving = true;
			}
		}

		if (missile_moving && missileInstance != null) {
			
			passed_time += Time.deltaTime;
			//Debug.Log (passed_time);
			ratio = passed_time/time_to_target;
			//Debug.Log("ratio: " + ratio);
			
			//Object is moved towards the target, depended on the ratio
			missileInstance.transform.position = new Vector3(Interpolations.interpolBezier(start.x, target.x, controlPointX, ratio),
			                                                Interpolations.interpolBezier(start.y, target.y, controlPointY, ratio),
			                                                Interpolations.interpolBezier(start.z, target.z, controlPointZ, ratio));
			
			missileInstance.transform.LookAt(new Vector3(Interpolations.interpolBezier(start.x, target.x, controlPointX, ratio + 0.01F),
			                                            Interpolations.interpolBezier(start.y, target.y, controlPointY, ratio + 0.01F),
			                                            Interpolations.interpolBezier(start.z, target.z, controlPointZ, ratio + 0.01F)));
			
			if(passed_time >= time_to_target){
				missile_moving = false;
				missileInstance.transform.position = target;
				Debug.Log("Nuke goes BUUUUM");
				GameObject obj = Instantiate(nukeExplosion,missileInstance.transform.position,Quaternion.identity) as GameObject;
				GameObject.Destroy(missileInstance);

				GameObject hq = VoronoiFieldCalculatorScript.instance.getHq (nukedHq);
				if(hq != null){
					hq.GetComponent<Unit>().hp = 0;
				}
			}
		}
	}

	public void NukeIt(){

		if (NetworkManager.instance.networkMode == NetworkManager.NetworkMode.hotSeat) {
			NukeItNormal ();
		} else {
			NukeItRPC ();
		}
	}

	public void NukeItNormal(){
		if (GameSupervisor.instance.faction == Factions.none || VoronoiFieldCalculatorScript.instance.getHq (Factions.red) == null || VoronoiFieldCalculatorScript.instance.getHq (Factions.blue) == null) {
			Debug.Log("Nuke missing faction/hq");
			return;
		}
		Camera.main.GetComponent<AudioSource> ().PlayOneShot (nukeStartSound);

		GameSupervisor.instance.resourceCurrent -= Settings.NUKE_COST;
		GameSupervisor.instance.nukeFired = true;
		passed_time = 0F;
		ratio = 0F;
		Debug.Log("Activate Nuke");
		nukeOrigPosition = new Vector3(submarine.transform.position.x, submarine.transform.position.y - 5, submarine.transform.position.z);
		missileInstance = (GameObject)GameObject.Instantiate (missile, nukeOrigPosition, Quaternion.Euler(new Vector3(270,0,0)));
		start = missileInstance.transform.position;

		NukeWatcher.instance.watch (missileInstance);
		submarine.GetComponentInChildren<Animator> ().SetTrigger ("Fight");

		if (GameSupervisor.instance.faction == Factions.blue) {
			target = VoronoiFieldCalculatorScript.instance.getHq(Factions.red).transform.position;
			nukedHq = Factions.red;
		}
		else if(GameSupervisor.instance.faction == Factions.red){
			target = VoronoiFieldCalculatorScript.instance.getHq(Factions.blue).transform.position;
			nukedHq = Factions.blue;
		}
		
		sub_moving = false;
		
		controlPointX = missileInstance.transform.position.x + missileInstance.transform.forward.x * 182.6842F;
		controlPointY = missileInstance.transform.position.y + missileInstance.transform.forward.y * 182.6842F;
		controlPointZ = missileInstance.transform.position.z + missileInstance.transform.forward.z * 182.6842F;
	}

	public void NukeItRPC(){
		GetComponent<NetworkView> ().RPC ("_RPC_NukeItRPC", RPCMode.All);
	}
	[RPC]
	void _RPC_NukeItRPC(){
		NukeItNormal ();
	}
}
