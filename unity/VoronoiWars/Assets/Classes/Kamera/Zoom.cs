﻿using UnityEngine;
using System.Collections;

public class Zoom : MonoBehaviour {

	public float speed = 1.2f;
	private Camera cam;

	void Start () 
	{
		cam = this.transform.GetComponent<Camera>();
	}
	
	// Update is called once per frame
	void Update ()
	{

		if (Input.GetAxis ("Mouse ScrollWheel") > 0)
		{
			if (cam.fieldOfView > 20)
			{
				cam.fieldOfView -= speed;
			}
		}

			
		if (Input.GetAxis ("Mouse ScrollWheel") < 0)
		{
			if(cam.fieldOfView < 70)
				{
				cam.fieldOfView += speed;
				}
		}
	}
}