﻿using UnityEngine;
using System.Collections;

public class Fokus : MonoBehaviour {

	public  Transform target;

	private float dist = -30.0f;
	private float lift = 20.2f;

	// Update is called once per frame
	void Update ()
	{
		if (target != null)
		{
			transform.position = target.position + new Vector3 (0, lift, dist);
			transform.LookAt (target.position);	
		}
	}
}
