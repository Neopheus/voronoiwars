﻿using UnityEngine;
using System.Collections;

public class screenscroll : MonoBehaviour {

	public Transform IslandCenterRed;
	public Transform IslandCenterBlue;
	private Transform IslandCenter;
	public float maxDistToCenter = 100;

	public int dist = 20; // distance to screen edge (starting scroll)
	public float speed; // scroll movement

	private const float origSpeed = 100;
	
	public float minX = 0;
	public float maxX = 0;
	public float minZ = 0;
	public float maxZ = 0;

	private int screenWidth;
	private int screenHeight;
	private Transform myTransform;

	private Vector3 temp = new Vector3();

	private Zoom zoom;
	private Camera camgirl;
	private Fokus fokus;

	Quaternion quat;

	private float fov;
	// Use this for initialization
	void Start ()
	{


		quat = Quaternion.Euler (new Vector3 (0, -90, 0));

		myTransform = this.transform;
	
		screenWidth = Screen.width;
		screenHeight = Screen.height;

		if (GetComponent<Camera>() != null)
		{
			camgirl = GetComponent<Camera>();
			fov = camgirl.fieldOfView;

			minX = minX + camgirl.transform.position.x;
			maxX = maxX + camgirl.transform.position.x;
			minZ = minZ + camgirl.transform.position.z;
			maxZ = maxZ + camgirl.transform.position.z;
		}

		if (GetComponent<Fokus> () != null)
		{
			fokus = GetComponent<Fokus> ();
		}
	}
	
	// Update is called once per frame
	void Update ()
	{
		//added by Bernhard (to fix screenresize issues while playing)
		screenWidth = Screen.width;
		screenHeight = Screen.height;


		if (camgirl.fieldOfView < fov) {
			speed = origSpeed * (camgirl.fieldOfView / 60);
		} else if (camgirl.fieldOfView == fov) {
			speed = origSpeed;
		} else if (camgirl.fieldOfView > fov) {
			speed = origSpeed * (camgirl.fieldOfView / 60);
		}



		if (camgirl.transform.position.x < maxX && (Input.mousePosition.x > screenWidth - dist || Input.GetKey (KeyCode.RightArrow) || Input.GetKey (KeyCode.D))) {
					
			if (GetComponent<Fokus> () != null) {
	
				fokus.enabled = false;
			}
		
			temp = myTransform.position;
			temp += myTransform.right * speed * Time.deltaTime;
			myTransform.position = temp;
		}

		if (camgirl.transform.position.x > minX && (Input.mousePosition.x < 0 + dist || Input.GetKey (KeyCode.LeftArrow) || Input.GetKey (KeyCode.A))) {			
			
				if (GetComponent<Fokus> () != null) {
					fokus.enabled = false;
				}
				temp = myTransform.position;
				temp -= myTransform.right * speed * Time.deltaTime;
				myTransform.position = temp;
			}


		if (camgirl.transform.position.z < maxZ && (Input.mousePosition.y > screenHeight - dist || Input.GetKey (KeyCode.UpArrow) || Input.GetKey (KeyCode.W))) {
				
				if (GetComponent<Fokus> () != null) {
					fokus.enabled = false;
				}
				temp = myTransform.position;
				temp += (quat * myTransform.right) * speed * Time.deltaTime;
				myTransform.position = temp;

			}


		if (camgirl.transform.position.z > minZ && (Input.mousePosition.y < 0 + dist || Input.GetKey (KeyCode.DownArrow) || Input.GetKey (KeyCode.S))) {
			
				if (GetComponent<Fokus> () != null) {
					fokus.enabled = false;
				}	
				temp = myTransform.position;
				temp -= (quat * myTransform.right) * speed * Time.deltaTime; 
				myTransform.position = temp;

				
			}


		IslandCenter = IslandCenterBlue;
		if (NetworkManager.instance.networkMode == NetworkManager.NetworkMode.hotSeat) {
			if (GameSupervisor.instance.faction == Factions.red) {
				IslandCenter = IslandCenterRed;
			}
		} else {
			if (GameSupervisor.instance.ownFaction == Factions.red) {
				IslandCenter = IslandCenterRed;
			}
		}


		Vector3 toCenter = this.transform.position - IslandCenter.position;
		toCenter.y = 0;
		if (toCenter.magnitude > maxDistToCenter) {
			toCenter.Normalize ();
			toCenter *= maxDistToCenter;
			toCenter.y = this.transform.position.y;
			this.transform.position = IslandCenter.position + toCenter;
		}
	}
}