﻿using UnityEngine;
//using Mathf.PI;
//using Mathf.Cos;
using System.Collections;

public class Interpolations{
	// implements interpolations
	
	// t has to be between 0 and 1.0
	private static float clampT(float t) {
		if (t > 1.0)
			t = 1.0f;
		if (t < 0.0)
			t = 0.0f;
		return t;
	}

	// One-dimensional linear interpolation
	public static float interpolLinear(float start, float end, float t) {
		t = clampT(t);
		float res = start + (end - start) * t;
		return res;
	}
	
	// One-dimensional bezier interpolation
	public static float interpolBezier(float start, float end, float control, float t) {
		t = clampT(t);
		float start_contr = interpolLinear(start, control, t);
		float contr_end = interpolLinear(control, end, t);
		float res = interpolLinear(start_contr, contr_end, t);
		return res;
	}

	// One-dimensional cosine interpolation
	public static float interpolCos(float start, float end, float t) {
		t = clampT(t);
		float delta_start_end = end - start;
		float res = (float) Mathf.Cos(t * Mathf.PI);
		// transform res to range between 1 and 0
		res = (1f + res) * 0.5f;
		// transform res to range between start and end
		res = end - (delta_start_end * res);
		return res;
	}
	
	// One-dimensional cubic interpolation
	public static float interpolCubic(float pre_a, float a, float b, float post_b, float t) {
		//magic, do not look
		float res = a + 0.5f * t 
			* (b - pre_a + t * (2.0f * pre_a - 5.0f * a + 4.0f * b - post_b + t
		   	* (3.0f * (a - b) + post_b - pre_a)));
		return res;
	}
}
