﻿using UnityEngine;
using System.Collections;

//Use this class to move a object freely towards the position of another target object
//Objects must be set in inspector
public class MoveFree : MonoBehaviour {
	
	//TODO: testtargetObject only for testing, change to Voronoifield
	public GameObject testtargetObject;

	public GameObject object_to_move;
	private Vector3 start;
	private Vector3 target;
	private bool moving = false;

	private float controlPointX;
	private float controlPointY;
	private float controlPointZ;

	//the time since starting the movement
	private static float passed_time;
	//the amount of time after which a unit reaches it's target position
	private static float time_to_target = 5;
	//ratio of the passed time compared to complete movement time
	private float ratio;
	
	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		if (moving) {
			target = testtargetObject.transform.position;

			passed_time += Time.deltaTime;
			//Debug.Log (passed_time);
			ratio = passed_time/time_to_target;
			//Debug.Log("ratio: " + ratio);

			//Object is moved towards the target, depended on the ratio
			object_to_move.transform.position = new Vector3(Interpolations.interpolBezier(start.x, target.x, controlPointX, ratio),
			                                                Interpolations.interpolBezier(start.y, target.y, controlPointY, ratio),
			                                                Interpolations.interpolBezier(start.z, target.z, controlPointZ, ratio));

			object_to_move.transform.LookAt(new Vector3(Interpolations.interpolBezier(start.x, target.x, controlPointX, ratio + 0.01F),
			                                            Interpolations.interpolBezier(start.y, target.y, controlPointY, ratio + 0.01F),
			                                            Interpolations.interpolBezier(start.z, target.z, controlPointZ, ratio + 0.01F)));

			if(passed_time >= time_to_target){
				moving = false;
				object_to_move.transform.position = target;
				Debug.Log("End movement");
			}
		}
	}

	//initiates the movement
	public void move(){
		passed_time = 0F;
		ratio = 0F;
		Debug.Log("Start movement");
		start = object_to_move.transform.position;
		moving = true;

		controlPointX = object_to_move.transform.position.x + object_to_move.transform.forward.x * 20F;
		controlPointY = object_to_move.transform.position.y + object_to_move.transform.forward.y * 20F;
		controlPointZ = object_to_move.transform.position.z + object_to_move.transform.forward.z * 20F;
	}
}
