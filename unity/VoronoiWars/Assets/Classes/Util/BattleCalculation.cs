using UnityEngine;
using System.Collections.Generic;

/*
 * BattleCalculation is used to calculate the battle between two units
 * provides methods to:
 * 	...
 * 
 * with bonus=2 and a attk==hp you get oneHit behavior on disadvantaged enemys and you need 2 units for a advantaged enemy (2 air kill 1 antiair)
 * 
 * Author: Bernhard Maier
 */

public class BattleCalculation {

	//default constructor
	public BattleCalculation(){}
	
	public static void calculateBattle(Unit attacker, Unit defender)
	{
		const float firstStrikeBonus = 1;
		const float noBonus = 1;
		const float increased = 2;
		const float decreased = 1/increased;
		
		float bonus = noBonus;
		
		int deathOfAttacker = 0;
		int deathOfDefender = 0;
		
		switch(attacker.unitType) {
		case UnitType.antiAir:
			switch(defender.unitType) {
			case UnitType.antiAir:
				bonus = noBonus;
				break;
			case UnitType.heli:
				bonus = increased;
				break;
			case UnitType.tank:
				bonus = decreased;
				break;
			case UnitType.hq:
				bonus = noBonus;
				break;
			}
			break;
		case UnitType.heli:
			switch(defender.unitType) {
			case UnitType.antiAir:
				bonus = decreased;
				break;
			case UnitType.heli:
				bonus = noBonus;
				break;
			case UnitType.tank:
				bonus = increased;
				break;
			case UnitType.hq:
				bonus = noBonus;
				break;
			}
			break;
		case UnitType.tank:
			switch(defender.unitType) {
			case UnitType.antiAir:
				bonus = increased;
				break;
			case UnitType.heli:
				bonus = decreased;
				break;
			case UnitType.tank:
				bonus = noBonus;
				break;
			case UnitType.hq:
				bonus = noBonus;
				break;
			}
			break;
		case UnitType.hq:
			switch(defender.unitType) {
			case UnitType.antiAir:
				bonus = noBonus;
				break;
			case UnitType.heli:
				bonus = noBonus;
				break;
			case UnitType.tank:
				bonus = noBonus;
				break;
			case UnitType.hq:
				bonus = noBonus;
				break;
			}
			break;
		}	

		deathOfDefender=Mathf.CeilToInt(defender.hp/(attacker.atk*bonus*firstStrikeBonus));
		deathOfAttacker=Mathf.CeilToInt(attacker.hp/(defender.atk));

		if(deathOfAttacker>deathOfDefender)
		{
			attacker.hp = attacker.hp - (int)(defender.atk*deathOfDefender);
			defender.hp = 0;
		}
		if(deathOfAttacker<deathOfDefender)
		{
			attacker.hp = 0;
			defender.hp = defender.hp - (int)(attacker.atk*bonus*firstStrikeBonus*deathOfAttacker);
		}
		if(deathOfAttacker==deathOfDefender)
		{
			attacker.hp = 0;
			defender.hp = 0;
		}

		if (Debug.isDebugBuild) {
			Debug.Log (
				"---BATTLECALCULATION---\n" +
				"Attacker.Type: " + attacker.unitType.ToString () + "\n" +
				"Defender.Type: " + defender.unitType.ToString () + "\n" +
				"Bonus: " + bonus + "\n" +
				"DeathOfAttacker: " + deathOfAttacker + "\n" +
				"DeathOfDefender: " + deathOfDefender + "\n" +
				"Attacker.HP: " + attacker.hp + "\n" +
				"Defender.HP: " + defender.hp + "\n" +
				"-----------------------");
		}
	}
}
