﻿using UnityEngine;
using System.Collections;

public class ResourceCalculations{
	
	//lower threshold on unit number
	private static float threshold_lower = 5.0f;
	//higher threshold on unit number
	private static float threshold_higher = 20.0f;
	//income factor at low threshold
	private static float thresh_low_income = 1.5f;
	//income factor at high threshold
	private static float thresh_high_income = 0.5f;

	//Unmodified costs per unit
	private static float base_cost_units = 10.0f;
	//Modifier per field away from spawn zone
	private static float distance_modifier = 0.5f;
	
	//Calculates unit factor between low threshold income and high t.i.
	private static float unit_factor_calc(int unit_number){
		float unit_factor;
		if (unit_number <= threshold_lower){
			unit_factor = thresh_low_income;
		}
		
		else if (unit_number < threshold_higher){
			//factor declines according to cosine interpolation
			float delta_th = threshold_higher - threshold_lower;
			unit_factor = 
				Interpolations.interpolCos(thresh_low_income,
				                           thresh_high_income,
				                           (float)(unit_number - threshold_lower)/delta_th);
			if (unit_factor < thresh_high_income){
				unit_factor = thresh_high_income;
			}  
		}
		
		else {
			unit_factor = thresh_high_income;
		}        
		return unit_factor;
	}
	
	//calculates income per round
	public static float income_formula(int tower_number, int round_number, int unit_number){
		float tower_factor = tower_number * Settings.TOWER_MULTI;
		float round_factor = Settings.ROUND_MULTI * round_number;
		float unit_factor = unit_factor_calc(unit_number);
		float income = Settings.BASE_INCOME + Settings.DYNAMIC_INCOME * tower_factor * round_factor /** unit_factor*/ + tower_number * Settings.BASE_INCOME_TOWER;
		return income;
	}

	//calculates costs for buying units dependend on distance to spawn zone
	public static float buy_unit_cost(int distance_to_spawn){
		float cost = base_cost_units * (float)(1 + distance_to_spawn * distance_modifier);
		return cost;
	}
}
