﻿using UnityEngine;
using System.Collections;

public class PanzerMoveTest : MonoBehaviour {
	public static bool iniDone = false;
	public GameObject camera;
	public GameObject smoke;

	// Use this for initialization
	void Start () {
		if (!iniDone) {
			iniDone = true;
//			for(int i = 0; i<100; i++){
//				GameObject go = (GameObject)GameObject.Instantiate(this.gameObject, new Vector3(Random.Range (-100, 100), 0, Random.Range (-100, 100)), Quaternion.identity);
//				go.transform.parent = this.transform.parent;
			//this.transform.GetChild(2).GetComponent<ParticleSystem>().enableEmission = false;
//			}
		}
	}
	
	// Update is called once per frame
	void Update () {
		//this.transform.position = this.transform.position + new Vector3 (0, 0, 5 * Time.deltaTime);
		//this.transform.LookAt (this.transform.parent.position);
		//this.transform.LookAt (camera.transform.position);
		//this.transform.RotateAround (camera.transform.position, new Vector3 (0, 1, 0), 5.01f);

		if (Input.GetKey ("w")) {
			this.transform.position = this.transform.position + this.transform.forward * 5 * Time.deltaTime;
			//this.transform.GetChild(2).GetComponent<ParticleSystem>().enableEmission = true;
		}

		if (Input.GetKey ("s")) {
			this.transform.position = this.transform.position + this.transform.forward * -5 * Time.deltaTime;
			//this.transform.GetChild(2).GetComponent<ParticleSystem>().enableEmission = false;
		}

		if (Input.GetKey ("a")) {
			this.transform.Rotate (new Vector3 (0,-1,0));
			//this.transform.GetChild(2).GetComponent<ParticleSystem>().enableEmission = true;
		}

		if (Input.GetKey ("d")) {
			this.transform.Rotate (new Vector3 (0,1,0));
			//this.transform.GetChild(2).GetComponent<ParticleSystem>().enableEmission = true;
		}
	}
}
