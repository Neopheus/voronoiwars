﻿using UnityEngine;
using System.Collections;

public class onClickSpawn : MonoBehaviour {
	Ray ray;
	RaycastHit hit;
	public GameObject prefabLeftMouse;
	public GameObject prefabRightMouse;
	public bool key0WasReleased = true;
	public bool key1WasReleased = true;

	public GameObject animatedOnject;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		ray=Camera.main.ScreenPointToRay(Input.mousePosition);
		if(Physics.Raycast(ray,out hit))
		{
			if(Input.GetKey(KeyCode.Mouse0) && key0WasReleased)
			{
				GameObject obj=Instantiate(prefabLeftMouse,new Vector3(hit.point.x,hit.point.y,hit.point.z), Quaternion.identity) as GameObject;
				key0WasReleased = false;
			}
			if(!Input.GetKey(KeyCode.Mouse0)){
				key0WasReleased = true;
			}
			if(Input.GetKey(KeyCode.Mouse1) && key1WasReleased)
			{
				GameObject obj=Instantiate(prefabRightMouse,new Vector3(hit.point.x,hit.point.y,hit.point.z), Quaternion.identity) as GameObject;
				key1WasReleased = false;
			}
			if(!Input.GetKey(KeyCode.Mouse1)){
				key1WasReleased = true;
			}
		}
	}
}
