﻿using UnityEngine;
using System.Collections;

public class TsarExplode : MonoBehaviour {
	public static bool iniDone = false;
	public float riseSpeed = 100;
	public float time = 0;
	public GameObject tsar;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		//ShroomInit
		tsar.transform.GetChild (2).Translate (Vector3.forward * riseSpeed * Time.deltaTime);
		//LightBall
		//tsar.transform.GetChild (5).Translate (Vector3.forward * riseSpeed * Time.deltaTime);

		//Shroom
		tsar.transform.GetChild (3).Translate (Vector3.forward * riseSpeed * Time.deltaTime);

		//FireRing
		tsar.transform.GetChild (12).Translate (Vector3.forward * riseSpeed * Time.deltaTime);
		
		time += Time.deltaTime;
		
		if (time >= 1.6f) {
			tsar.transform.GetChild (9).GetComponent<ParticleSystem> ().Clear ();
			tsar.transform.GetChild (4).GetComponent<Light> ().intensity -= Time.deltaTime * 0.75f;
		}
		
		if (riseSpeed >= 0) {
			riseSpeed -= Time.deltaTime * 0.2f;
		}

	}
}
