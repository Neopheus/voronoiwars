﻿using UnityEngine;
using System.Collections;

public class NukeExplode : MonoBehaviour {
	public float riseSpeed = 5;
	public int a = 0;
	public float time = 0;
	public GameObject nuke;


	// Use this for initialization
	void Start () {
		checkDistanceAndKill(GameObject.FindGameObjectsWithTag("UnitRed"));
		checkDistanceAndKill(GameObject.FindGameObjectsWithTag("UnitBlue"));
		TerrainDeformerAfterNuke.instance.explode (nuke.transform.position);
	}

	private void checkDistanceAndKill(GameObject[] units){
		for (int i = 0; i<units.Length; i++) {
			if(Vector3.Distance(units[i].transform.position, this.transform.position) < 60 && units[i].GetComponent<Unit>().unitType != UnitType.hq){
				units[i].GetComponent<Unit>().hp = 0;
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
		//cone
		nuke.transform.GetChild (0).Translate (Vector3.back * riseSpeed * Time.deltaTime);
		//ball
		nuke.transform.GetChild (1).Translate (Vector3.forward * riseSpeed * Time.deltaTime);
		//fireRing
		nuke.transform.GetChild (2).Translate (Vector3.forward * riseSpeed * Time.deltaTime);
		//cone2
		nuke.transform.GetChild (3).Translate (Vector3.forward * riseSpeed * Time.deltaTime);
		//Point light
		nuke.transform.GetChild (5).Translate (Vector3.up * riseSpeed * Time.deltaTime);
		//upperOuterRing
		nuke.transform.GetChild (6).Translate (Vector3.back * riseSpeed * Time.deltaTime);


		time += Time.deltaTime;

		if (time >= 1.6f) {
			nuke.transform.GetChild (7).GetComponent<ParticleSystem> ().Clear ();
			nuke.transform.GetChild (5).GetComponent<Light> ().intensity -= Time.deltaTime * 0.8f;
		}

		if (riseSpeed >= 0) {
			riseSpeed -= Time.deltaTime * 0.2f;
		}

	}



}
