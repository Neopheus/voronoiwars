﻿using UnityEngine;
using System.Collections.Generic;

/*
 * Pathfinding is used to calculate a way from field 'a' to field 'b' in a graph
 * provides methods to:
 * 	
 * Author: Bernhard Maier
 */

public class Pathfinding {

	//default constructor
	public Pathfinding(){}

	//graph contains a list of voronoiFields which create complete graph
	private static List<VoronoiField> graph = new List<VoronoiField>();
	
	//path contains a list of voronoiFields which create the shortest path
	private static List<VoronoiField> path = new List<VoronoiField>();
	
	//status[] is used to store the status of every node in the given graph using the status enum
	private static Status[] status;
	
	//distances[] is used to store the shortest distance from the start to every node in the given graph
	private static float[] distance;
	
	//previousFields[] is used to store the prevoius node of the shortest way for every node in the given graph
	private static VoronoiField[] previousNode;

	//nextNode contains the next node to use for the update
	private static VoronoiField nextNode;

	//containsOpenStatus is true as long there are any nodes with open status in the graph, if it's false, the pathfinding is finished
	private static bool containsOpenStatus;

	//possible status of a node
	public enum Status {OPEN, CLOSED};

	//calculates the shortest way with the dijkstra algorithem
	public static List<VoronoiField> calculatePath(List<VoronoiField> partialGraph, VoronoiField startNode, VoronoiField endNode)
	{
		partialGraph = VoronoiFieldCalculatorScript.instance.sideCenters;
		if(partialGraph.Contains(startNode) && partialGraph.Contains(endNode))
		{
			//initializing all variables
			initialize(partialGraph, startNode);

			//updating the graph
			while(containsOpenStatus)
			{
				nextNode = getSmallestOpenNode();
				updateGraph(nextNode);
				checkStatus();
			}

			//creating the path
			createPath(endNode);
			path.Reverse();

			//returning the path
			if(path.Count>0)
			{
				return path;
			}
			else
			{
				Debug.Log("path is empty!");
				return null;
			}
		}
		else
		{
			Debug.Log("start or end node is not in graph!");
			return null;
		}

	}

	//initializing the previous fields with null and all distances with float.MaxValue, except the startpoint with '0'
	private static void initialize(List<VoronoiField> partialGraph, VoronoiField startNode)
	{
		graph = partialGraph;
		path = new List<VoronoiField>();
		status = new Status[graph.Count];
		distance = new float[graph.Count];
		previousNode = new VoronoiField[graph.Count];
		nextNode = null;
		containsOpenStatus = true;
		
		for(int i = 0; i < graph.Count; ++i)
		{
			if(graph[i] == startNode)
			{
				status[i] = Status.OPEN;
				distance[i] = 0;
				previousNode[i] = null;
			}
			else
			{
				status[i] = Status.OPEN;
				distance[i] = float.MaxValue;
				previousNode[i] = null;
			}
		}
	}

	//returns the node with status "open" and the smallest distance
	private static VoronoiField getSmallestOpenNode()
	{
		VoronoiField smallestOpenNode = null;
		float tmpDistance = float.MaxValue;

		for (int i = 0; i < graph.Count; ++i)
		{
			if(status[i]==Status.OPEN && distance[i]<tmpDistance)
			{
				tmpDistance = distance[i];
				smallestOpenNode = graph[i];
			}
		}

		status[getNodeIndex(smallestOpenNode)] = Status.CLOSED;
		return smallestOpenNode;
	}

	//updates the graph, calculates the distances from given node to its neighbors
	private static void updateGraph(VoronoiField currentNode)
	{
		for(int i = 0; i < currentNode.getNeighbors().Count; ++i)
		{
			VoronoiField neighbor = currentNode.getNeighbors()[i];

			if(graph.Contains(neighbor))
			{
				float tmpDistance = RangeDetection.getDistanceByNeighborrelation(currentNode, neighbor) + distance[getNodeIndex(currentNode)];

				if(tmpDistance < distance[getNodeIndex(neighbor)])
				{
					distance[getNodeIndex(neighbor)] = tmpDistance;
					previousNode[getNodeIndex(neighbor)] = currentNode;
				}
			}
		}
	}

	//get the index of a node in a given graph
	private static int getNodeIndex(VoronoiField node)
	{
		for (int i = 0; i < graph.Count; ++i)
		{
			if(graph[i] == node) return i;
		}

		return 0;
	}

	//checks if there are nodes with the status "open" are left and updates the containsOpenStatus variable
	private static void checkStatus()
	{
		containsOpenStatus = false;

		for(int i = 0; i < graph.Count; ++i)
		{
			if(status[i] == Status.OPEN)
			{
				containsOpenStatus = true;
				break;
			}
		}
	}

	//recursive creation of the path using the previousNodes array, node at the first call will be the end node
	private static List<VoronoiField> createPath(VoronoiField node)
	{
		if(previousNode[getNodeIndex(node)] == null)
		{
			path.Add(node);
			return path;
		}
		else
		{
			path.Add(node);
			return createPath(previousNode[getNodeIndex(node)]);
		}
	}

	//prints the current state to the debug console
	private static void printCurrentState()
	{
		for(int i = 0; i < graph.Count; ++i)
		{
			Debug.Log (i + "   |   " + graph[i].calculateCentroidAsString() + "   |   " + status[i] + "   |   " + distance[i] + "   |   " + "previousNode[i].toString()");
		}
	}

}
