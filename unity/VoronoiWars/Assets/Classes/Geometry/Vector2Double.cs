using System;
/* 	
 * Author: Jakob Schaal
 */
public struct Vector2Double
{
	public static Vector2Double zero{
		get{
			return new Vector2Double(0, 0);
		}
	}

	public static double Distance(Vector2Double a, Vector2Double b){
		Vector2Double pointVec = a - b;
		return pointVec.magnitude;
	}
	public static double DistanceSq(Vector2Double a, Vector2Double b){
		Vector2Double pointVec = a - b;
		return pointVec.sqrMagnitude;
	}


	public double x;
	public double y;

	public double sqrMagnitude{
		get{
			return x*x + y*y;
		}
	}
	public double magnitude{
		get{
			return Math.Sqrt(sqrMagnitude);
		}
	}

	public Vector2Double normalized{
		get{
			double len = magnitude;
			return new Vector2Double(x/len, y/len);
		}
	}

	public Vector2Double(double x, double y){
		this.x = x;
		this.y = y;
	}

	public void Normalize(){
		double len = magnitude;
		x /= len;
		y /= len;
	}

	public static Vector2Double operator +(Vector2Double a, Vector2Double b){
		return new Vector2Double(a.x + b.x, a.y + b.y);
	}
	
	public static Vector2Double operator -(Vector2Double a, Vector2Double b){
		return new Vector2Double(a.x - b.x, a.y - b.y);
	}
	
	public static Vector2Double operator *(Vector2Double a, double b){
		return new Vector2Double(a.x * b, a.y * b);
	}
	
	public static Vector2Double operator /(Vector2Double a, double b){
		return new Vector2Double(a.x / b, a.y / b);
	}

	public static bool operator !=(Vector2Double a, Vector2Double b){
		return a.x != b.x || a.y != b.y;
	}
	
	public static bool operator ==(Vector2Double a, Vector2Double b){
		return a.x == b.x && a.y == b.y;
	}
	

}

