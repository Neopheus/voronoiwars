﻿using UnityEngine;
using System.Collections;

/* 	
 * Author: Jakob Schaal
 */
public class VoronoiSide : System.IComparable<VoronoiSide>{
	public readonly Vector2Double side;

	public VoronoiSide(){
		side = Vector2Double.zero;
	}
	public VoronoiSide(Vector2Double side){
		this.side = side;
	}
	public VoronoiSide(double x, double y){
		side = new Vector2Double (x, y);
	}

	public int CompareTo(VoronoiSide other){
		if (side.y > other.side.y) {
			return -1;
		} else if (side.y < other.side.y) {
			return 1;
		} else if (side.x > other.side.x) {
			return -1;
		} else {
			return 0;
		}
	}
}
