﻿using UnityEngine;
using System.Collections;

/* 	
 * Author: Jakob Schaal
 */
public class ValueNoise {
	private readonly int WIDTH;
	private readonly int HEIGHT;
	private float[,] values;
	private int octaves = 5;
	private float persistence = 0.25f;
	private float startAmplitude = 1f;
	private int startFrequencyX = 4;
	private int startFrequencyY = 4;
	private Interpolations interpol = new Interpolations ();

	public ValueNoise(int width, int height){
		WIDTH = width;
		HEIGHT = height;
		values = new float[WIDTH, HEIGHT];
		calculate ();
	}

	private void calculate(){
		float[] amplitudes = new float[octaves];
		float sumOfAmplitudes = startAmplitude;
		int[] frequencysX = new int[octaves];
		int[] frequencysY = new int[octaves];

		amplitudes [0] = startAmplitude;
		frequencysX [0] = startFrequencyX;
		frequencysY [0] = startFrequencyY;

		for(int i = 1; i<octaves; i++){
			amplitudes[i] = amplitudes[i-1]*persistence;
			frequencysX[i] = frequencysX[i-1] * 2;
			frequencysY[i] = frequencysY[i-1] * 2;
			sumOfAmplitudes += amplitudes[i];
		}

		for(int i = 0; i<WIDTH; i++){
			for(int k = 0; k<HEIGHT; k++){
				values[i, k] = 0;
			}
		}
		
		
		
		for(int i = 0; i < octaves; i++){
			float[,] discretePoints = new float[frequencysX[i] + 4, frequencysY[i] +4];
			for(int m = 0; m<frequencysX[i] + 4; m++){
				for(int n = 0; n<frequencysY[i] +4; n++){
					discretePoints[m, n] = Random.Range(0f, 1f) * amplitudes[i];
				}
			}
			
			
			for(int k = 0; k<WIDTH; k++){
				float positionX = k/((float)WIDTH) * frequencysX[i] + 2;
				int currentPosX = (int) positionX;
				float x = positionX - currentPosX;
				
				for(int m = 0; m<HEIGHT; m++){
					
					float positionY = m/((float)HEIGHT) * frequencysY[i] + 2;
					int currentPosY = (int) positionY;
					
					
					
					float y = positionY - currentPosY;
					
					//If we need another inpolation mode then we just can update the interpolate methode and dont have to touch
					//the next 4 chunk lines. 
					float preA  = interpolate(discretePoints[currentPosX-1, currentPosY-1], discretePoints[currentPosX-1, currentPosY], discretePoints[currentPosX-1, currentPosY+1], discretePoints[currentPosX-1, currentPosY+2], y);
					float a     = interpolate(discretePoints[currentPosX,   currentPosY-1]  , discretePoints[currentPosX, currentPosY]  , discretePoints[currentPosX, currentPosY+1]  , discretePoints[currentPosX, currentPosY+2]  , y);
					float b     = interpolate(discretePoints[currentPosX+1, currentPosY-1], discretePoints[currentPosX+1, currentPosY], discretePoints[currentPosX+1, currentPosY+1], discretePoints[currentPosX+1, currentPosY+2], y);
					float postB = interpolate(discretePoints[currentPosX+2, currentPosY-1], discretePoints[currentPosX+2, currentPosY], discretePoints[currentPosX+2, currentPosY+1], discretePoints[currentPosX+2, currentPosY+2], y);
					
					values[k, m] += interpolate(preA, a, b, postB, x);
					
				}
			}
		}
		
		
		
		
		
		for(int i = 0; i<WIDTH; i++){
			for(int k = 0; k<HEIGHT; k++){
				values[i, k] /= sumOfAmplitudes;
				values[i, k] *= startAmplitude;
			}
		}


		for (int i = 0; i<WIDTH; i++) {
			for(int k = 0; k<HEIGHT; k++){
				values[i,k] /= sumOfAmplitudes;
				values[i,k] *= startAmplitude;
			}
		}

		float maxHeight = 0;
		for (int i = 0; i<WIDTH; i++) {
			for (int k = 0; k<HEIGHT; k++) {
				if(values[i, k] > maxHeight) maxHeight = values[i, k];
			}
		}
		
		for (int i = 0; i<WIDTH; i++) {
			for (int k = 0; k<HEIGHT; k++) {
				values[i, k] /= maxHeight;
				values[i, k] *= startAmplitude;
			}
		}
	}

	private float interpolate(float preA, float a, float b, float postB, float t){
		return Interpolations.interpolCubic (preA, a, b, postB, t);
	}

	private static float linear(float a, float b, float x){
		return (b * x) + (a * (1-x));
	}

	public float getValue(int x, int y){
		if(x < 0 || x >= WIDTH) throw new UnityException("The locationX must be between 0 and " +(WIDTH-1)+" but it was "+x);
		if(y < 0 || y >= HEIGHT) throw new UnityException("The locationY must be between 0 and " +(HEIGHT-1)+" but it was "+y);


		return values[x,y];
	}
}
