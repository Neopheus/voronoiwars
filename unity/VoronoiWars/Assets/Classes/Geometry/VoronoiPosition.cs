using UnityEngine;
using System.Collections.Generic;
/* 	
 * Author: Jakob Schaal
 */
public class VoronoiPosition : System.IComparable<VoronoiPosition>, System.IComparable<VoronoiCircleEvent>, System.IComparable<VoronoiField>
{
	public Vector2Double pos;
	public VoronoiPosition(){
		pos = Vector2Double.zero;
	}
	public VoronoiPosition(Vector2Double pos){
		this.pos = pos;
	}
	public VoronoiPosition(double x, double y){
		pos = new Vector2Double (x, y);
	}
	
	public int CompareTo(VoronoiPosition other){
		Vector2Double thisPos = this.pos;
		Vector2Double otherPos = other.pos;

		if (this is VoronoiCircleEvent) {
			thisPos.x = (double)((VoronoiCircleEvent)this).triggerPosition;
		}
		if (other is VoronoiCircleEvent) {
			otherPos.x = (double)((VoronoiCircleEvent)other).triggerPosition;
		}

		if (this is VoronoiCircleEvent && other is VoronoiField) {
			VoronoiCircleEvent vce = (VoronoiCircleEvent)this;
			if(vce.triggerPosition >= other.pos.x) return 1;
		}
		if (this is VoronoiField && other is VoronoiCircleEvent) {
			VoronoiCircleEvent vce = (VoronoiCircleEvent)other;
			if(vce.triggerPosition >= this.pos.x) return -1;
		}

		if (other == null) {
			return -1;
		}
		if (thisPos.x > otherPos.x) {
			return 1;
		} else if (thisPos.x < otherPos.x) {
			return -1;
		} else if (thisPos.y > otherPos.y) {
			return 1;
		} else if (thisPos.y < otherPos.y) {
			return -1;
		}
		else {
			return 0;
		}
	}

	public int CompareTo(VoronoiCircleEvent vce){
		return CompareTo ((VoronoiPosition)vce);
	}

	public int CompareTo(VoronoiField vf){
		return CompareTo ((VoronoiPosition)vf);
	}
}

