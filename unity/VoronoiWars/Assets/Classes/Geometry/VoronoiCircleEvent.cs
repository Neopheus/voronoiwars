﻿using UnityEngine;
using System.Collections;

/*
 * Represents a circle
 * Author: Jakob Schaal
 */
public class VoronoiCircleEvent : VoronoiPosition{

	public double triggerPosition;
	public ParabolicArc a;
	public bool valid;

	public VoronoiCircleEvent(double triggerPosition, Vector2Double v, ParabolicArc a){
		pos = v /*+ new Vector2 (0, (float)triggerPosition)*/;
		this.triggerPosition = triggerPosition;
		this.pos = v;
		this.a = a;
		valid = true;
	}
}