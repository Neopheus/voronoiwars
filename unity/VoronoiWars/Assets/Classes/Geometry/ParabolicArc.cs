﻿using UnityEngine;
using System.Collections;
using System;

/*
 * Author: Jakob Schaal
 */
public class ParabolicArc {
	public static ParabolicArc root = null;

	public Vector2Double spawnPoint;
	public ParabolicArc previous = null;
	public ParabolicArc next = null;
	public Line2 l1 = null;
	public Line2 l2 = null;
	private bool didOpen = false;

	private double a;
	private double b;
	private double c;
	private double lastCalculatedSweepVal;

	private bool tempArc = false;

	public VoronoiCircleEvent e = null;

	public ParabolicArc(Vector2Double v){
		this.spawnPoint = v;
		lastCalculatedSweepVal = v.x;
	}
	public ParabolicArc(Vector2Double v, ParabolicArc previous){
		this.spawnPoint = v;
		this.previous = previous;
		lastCalculatedSweepVal = v.x;
	}
	public ParabolicArc(Vector2Double v, ParabolicArc previous, ParabolicArc next){
		this.spawnPoint = v;
		this.previous = previous;
		this.next = next;
		lastCalculatedSweepVal = v.x;
	}

	public void calculateABC(double sweepVal){
		if (tempArc)
			throw new UnityException ("You can't calculate ABC on a temp arc!");
		lastCalculatedSweepVal = sweepVal;
		if (sweepVal > spawnPoint.x) {
			didOpen = true;
			double helpVal = spawnPoint.x - sweepVal;
			a = 1 / (2 * (helpVal));
			b = -2 * ((spawnPoint.y) / (2 * (helpVal)));
			c = (spawnPoint.y * spawnPoint.y + spawnPoint.x * spawnPoint.x - sweepVal * sweepVal) / (2 * (helpVal));
		} else {
			didOpen = false;
		}
	}


	//true if a new Parabola at spawnPoint would intersect with the beachline part of this parabola, else false
	public bool wouldNewParabolaIntersect(Vector2Double spawnPoint){
		if (this.spawnPoint.x == spawnPoint.x)
			return false;

		double m = 0;
		double n = 0;

		if (previous != null) {
			m = previous.intersect(this, spawnPoint.x).y;
		}
		if (next != null) {
			n = this.intersect(next, spawnPoint.x).y;
		}

		if(((previous == null)||(m <= spawnPoint.y))&&((next == null)||(n >= spawnPoint.y))){
			return true;
		}

		return false;
	}

	//return the intersection point of a new parabola at other and this parabola
	public Vector2Double intersect(Vector2Double other){
		Vector2Double returnValue = Vector2Double.zero;
		returnValue.y = other.y;

		//langsam kann ich die Parabelgleichungen nicht mehr sehen >.<
		returnValue.x = (this.spawnPoint.x * this.spawnPoint.x - other.x*other.x + (this.spawnPoint.y - other.y)*(this.spawnPoint.y - other.y)) / (2 * this.spawnPoint.x - 2 * other.x);
		return returnValue;
	}
	
	public Vector2Double intersect(ParabolicArc other, double sweepVal){
		Vector2Double returnValue = Vector2Double.zero;

		Vector2Double p = spawnPoint;
		if (spawnPoint.x == other.spawnPoint.x) {
			returnValue.y = (spawnPoint.y + other.spawnPoint.y) / 2;
		} else if (other.spawnPoint.x == sweepVal) {
			returnValue.y = other.spawnPoint.y;
		} else if (spawnPoint.x == sweepVal) {
			p = other.spawnPoint;
			returnValue.y = this.spawnPoint.y;
		} else {
			this.calculateABC (sweepVal);
			other.calculateABC (sweepVal);
			ParabolicArc temp = this - other;
			double a = temp.a;
			double b = temp.b;
			double c = temp.c;
			returnValue.y = (double)(( -b - Math.Sqrt((double)(b*b - 4*a*c)) ) / (2*a));
		}

		returnValue.x = (double)((p.x*p.x + (p.y - returnValue.y)*(p.y - returnValue.y) - sweepVal*sweepVal)/(2 * p.x - 2 * sweepVal));
		return returnValue;
	}



	public static ParabolicArc operator - (ParabolicArc x, ParabolicArc y){
		equalizeSweepLine (x, y);

		ParabolicArc retVal = new ParabolicArc (Vector2Double.zero);
		retVal.a = x.a - y.a;
		retVal.b = x.b - y.b;
		retVal.c = x.c - y.c;
		retVal.tempArc = true;
		return retVal;
	}

	private static void equalizeSweepLine(ParabolicArc x, ParabolicArc y){
		if (x.lastCalculatedSweepVal > y.lastCalculatedSweepVal) {
			y.calculateABC (x.lastCalculatedSweepVal);
			x.calculateABC (x.lastCalculatedSweepVal);
		} else if (x.lastCalculatedSweepVal > y.lastCalculatedSweepVal) {
			x.calculateABC (y.lastCalculatedSweepVal);
			y.calculateABC (y.lastCalculatedSweepVal);
		}
	}

	public void insert(ParabolicArc newArc){
		newArc.previous = this;
		newArc.next = next;
		next.previous = newArc;
		next = newArc;
	}
}