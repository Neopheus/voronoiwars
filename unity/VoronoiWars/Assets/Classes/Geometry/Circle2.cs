﻿using UnityEngine;
using System.Collections;

/*
 * A Circle in 2D Space.
 * Author: Jakob Schaal
 */
public class Circle2 {
	public Vector2Double center;
	public double radius;

	public Circle2(Vector2Double center, double radius){
		this.center = center;
		this.radius = radius;
	}
	public Circle2(double x, double y, double radius){
		this.center = new Vector2Double (x, y);
		this.radius = radius;
	}
	public Circle2(double radius){
		center = Vector2Double.zero;
		this.radius = radius;
	}

	/*
	 * An sich trivial, aber falls jemand verwirrt ist was hier passiert:
	 * https://www.khanacademy.org/math/geometry/triangle-properties/perpendicular_bisectors/v/three-points-defining-a-circle
	 */
	public Circle2(Vector2Double a, Vector2Double b, Vector2Double c){
		double A = b.x - a.x;
		double B = b.y - a.y;
		double C = c.x - a.x;
		double D = c.y - a.y;
		double E = A * (a.x + b.x) + B*(a.y + b.y);
		double F = C * (a.x + c.x) + D*(a.y + c.y);
		double G = 2 * (A * (c.y - b.y) - B * (c.x - b.x));
		if(G == 0) throw new UnityException("Points are colinear!");

		center.x = (D * E - B * F) / G;
		center.y = (A * F - C * E) / G;

		radius = Vector2Double.Distance (center, a);


		/*Vector2 middleAB = (a + b) / 2;
		Vector2 perpAB = new Vector2(b.x - a.x, a.y - b.y);

		Vector2 middleBC = (b + c) / 2;
		Vector2 perpBC = new Vector2 (c.x - b.x, b.y - c.y);

		Vector2 middleCA = (c + a) / 2;
		Vector2 perpCA = new Vector2 (a.x - c.x, c.y - a.y);
		
		Line2 l1 = new Line2 (middleAB, middleAB + perpAB);
		Line2 l2 = new Line2 (middleBC, middleBC + perpBC);
		Line2 l3 = new Line2 (middleCA, middleCA + perpCA);

		center = l1.intersect (l2);
		radius = Vector2.Distance (center, a);

		if (float.IsInfinity (center.x) || float.IsInfinity (center.y)) {
			throw new UnityException("Points are colinear!");
		}*/
	}




	public bool intersects(Circle2 other){
		double radiSum = other.radius + this.radius;
		double radiSum2 = radiSum * radiSum;
		return (this.center - other.center).sqrMagnitude < radiSum2;
	}

/*	public static bool leftOfSweepLine(Vector2 a, Vector2 b, Vector2 c){
		if ((b.x-a.x)*(c.y-a.y) - (c.x-a.x)*(b.y-a.y) > 0)
			return false;
		return true;
	}*/
}
