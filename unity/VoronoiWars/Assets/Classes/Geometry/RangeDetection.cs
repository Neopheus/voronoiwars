﻿using UnityEngine;
using System.Collections.Generic;

/*
 * RangeDetection is used to calculate all voronoi fields which are in range of an unit
 * provides methods to:
 * 	calculate the range
 * 	add a field to a list
 * 	calculate the distance between two fields by the height difference
 * 	calculate the distance between two fields by the neighbor realtions
 * 	calculate the distance between two fields by the distance of the centeroids
 * Author: Bernhard Maier
 */

public class RangeDetection {

	private static VoronoiField startPosition;
	private static bool addable = true;

	//default constructor
	public RangeDetection(){}

	//calculates recursive all voronoifields in range x to a position
	public static List<VoronoiField> calculateRange(VoronoiField position, int range)
	{
		startPosition = position;
		List<VoronoiField> fieldsInRange = new List<VoronoiField>();
		fieldsInRange.Add(position);
		return _calculateRange(position, range, fieldsInRange);
	}

	//helperfunction for the rangecalculation
	private static List<VoronoiField> _calculateRange(VoronoiField position, int range, List<VoronoiField> fieldsInRange)
	{
		if (range <= 0)
		{
			return fieldsInRange;
		}
		else
		{
			List<VoronoiField> tmpNeighborsList = new List<VoronoiField>();
			for(int i = 0; i < position.getNeighbors().Count; ++i)
			{
				if(position.getNeighbors()[i].isPassable)
				{
					addToList(position.getNeighbors()[i],tmpNeighborsList);
				}
			}
			foreach(VoronoiField vf in tmpNeighborsList)
			{
				addable = true;
				if(vf.unit!=null)
				{
					if(vf.unit.faction==startPosition.unit.faction && vf!=startPosition)
					{
						addable=false;
					}
				}
				if(addable)
				{
					addToList(vf, fieldsInRange);
				}
				if(vf.unit==null)
				{
					_calculateRange(vf, range-1, fieldsInRange);
				}
			}
			return fieldsInRange;
		}
	}
	
	//adds a voronoifield to a list if it's not already in there
	static bool addToList(VoronoiField vf, List<VoronoiField> list)
	{
		if(!list.Contains(vf))
		{
			list.Add (vf);
			return true;
		}
		return false;
	}

	//used to calculate the height difference between two voronoi fields
	public static float getDistanceByHeight(VoronoiField a, VoronoiField b)
	{
		double distance = a.height - b.height;
		if(distance < 0)
		{
			distance *= -1;
		}
		if(a.unit != null || b.unit != null)
		{
			distance *= 1000;
		}
		return (float)distance;
	}
	
	//used to get a distance of 1 for neighboring voronoi fields
	public static float getDistanceByNeighborrelation(VoronoiField a, VoronoiField b)
	{
		float distance = 100;
		if(a.SupplyCrate != null || b.SupplyCrate != null)
		{
			distance = 99;
		}
		if(a.unit != null || b.unit != null || a.isBorderField || b.isBorderField)
		{
			distance = 10000;
		}
		return (float)distance;
	}
	
	//used to get the distance by the distance of the centroids of the voronoi fields
	public static float getDistanceByCentroids(VoronoiField a, VoronoiField b)
	{
		float distance = Vector3.Distance(a.worldPos, b.worldPos);
		if(a.unit != null || b.unit != null)
		{
			distance *= 1000;
		}
		return distance;
	}
}
