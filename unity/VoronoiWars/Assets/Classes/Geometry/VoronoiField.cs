﻿using UnityEngine;
using System;
using System.Collections.Generic;

/*
 * By definition a VoronoiField is a flat, closed, convex polygon.
 * Author: Jakob Schaal
 */
public class VoronoiField : VoronoiPosition{
	private List<Vector2Double> vertices;
	private int n;
	public int index;
	private List<VoronoiField> neighbors = new List<VoronoiField>();
	public double height = UnityEngine.Random.Range (0f, 1f);
	public GameObject light;
	public GameObject pumpJack;
	public Unit unit = null;

	public bool isBorderField = false;

	public int distanceToRed = -1;
	public int distanceToBlue = -1;

	public int id;

	public GameObject SupplyCrate;

	public bool orginalSpawnField = false;

	public bool isPassable{
		get{
			return !isBorderField;
		}
	}
	public Factions playerField = Factions.none;

	public Vector3 worldPos{
		get{
			return new Vector3((float)(pos.y / VoronoiFieldCalculatorScript.size * 200), (float)(height*30), (float)(pos.x / VoronoiFieldCalculatorScript.size * 200));
		}
	}

	public VoronoiField(List<Vector2Double> vertices){
		n = vertices.Count;
		if(n < 3){
			throw new UnityException("There must be at least 3 vertices but there were " + n);
		}
		this.vertices = vertices;

		if (Debug.isDebugBuild) {
			checkValidity();
		}

		pos = calculateCentroid ();
	}

	public VoronoiField(Vector2Double centroid){
		n = 0;
		this.pos = centroid;
	}

	private void checkValidity(){
		checkConvex ();
	}

	public List<VoronoiField> getNeighbors(){
		return neighbors;
	}
	public void setNeighbors(List<VoronoiField> neighbors){
		this.neighbors = neighbors;
	}
	public void addNeighbor(VoronoiField neighbor){
		if (neighbors.Contains (neighbor))
			return;
		if (neighbor == null)
			throw new UnityException ("NULL");

		neighbors.Add (neighbor);
		neighbor.addNeighbor (this);
	}

	private void checkConvex(){
		int currentIndex  = 0;
		int nextIndex     = 1;
		int nextNextIndex = 2;
		VoronoiField.Orientation o = Orientation.NONE;

		for (currentIndex = 0; currentIndex < n; currentIndex++) {
			nextIndex     = (currentIndex + 1) % n;
			nextNextIndex = (currentIndex + 2) % n;

			Vector2Double currentVector  = vertices[currentIndex];
			Vector2Double nextVector     = vertices[nextIndex];
			Vector2Double nextNextVector = vertices[nextNextIndex];

			Vector2Double v1 = nextVector - currentVector;
			Vector2Double v2 = nextNextVector - nextVector;

			double dot = v1.x * -v2.y + v1.y*v2.x;	//test if left or right!

			if(currentIndex == 0){
				if(dot < 0){
					o = Orientation.CLOCKWISE;
				}
				else{
					o = Orientation.COUNTERCLOCKWISE;
				}
			}
			else{
				if(o == Orientation.CLOCKWISE && dot > 0){
					throw new UnityException("VoronoiField MUST be convex!");
				}
				else if(o == Orientation.COUNTERCLOCKWISE && dot < 0){
					throw new UnityException("VoronoiField MUST be convex!");
				}
			}
		}
	}
	private enum Orientation{
		NONE, CLOCKWISE, COUNTERCLOCKWISE	
	}



	public double calculateArea(){
		double area = 0;
		int nextI = 1;

		for (int i = 0; i<n; i++) {
			nextI = (i + 1) % n;
			Vector2Double v = vertices[i];
			Vector2Double w = vertices[nextI];
			area += v.x * w.y - v.y * w.x;
		}

		return Math.Abs (area * 0.5f);
	}

	public double calculateSignedArea(){
		double signedArea = 0;

		for (int i = 0; i<n-1; i++) {
			Vector2Double v = vertices[i];
			Vector2Double w = vertices[i+1];
			signedArea += v.x * w.y - w.x * v.y;
		}

		return signedArea * 0.5f;
	}

	public Vector2Double calculateCentroid(){
		double a = 1 / (6*calculateSignedArea ());
		double x = 0;
		double y = 0;
		
		for(int i = 0; i<n-1; i++){
			Vector2Double v = vertices[i];
			Vector2Double w = vertices[i+1];
			
			x += (v.x + w.x)*(v.x * w.y - w.x * v.y);
			y += (v.y + w.y)*(v.x * w.y - w.x * v.y);
		}
		
		return new Vector2Double (x*a, y*a);
	}
	
	//added by Bernhard
	public string calculateCentroidAsString(){
		double a = 1 / (6*calculateSignedArea ());
		double x = 0;
		double y = 0;
		
		for(int i = 0; i<n-1; i++){
			Vector2Double v = vertices[i];
			Vector2Double w = vertices[i+1];
			
			x += (v.x + w.x)*(v.x * w.y - w.x * v.y);
			y += (v.y + w.y)*(v.x * w.y - w.x * v.y);
		}
		
		return "("+(x*a)+"|"+(y*a)+")";
	}
}




