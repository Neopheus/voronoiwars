﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
/*
 * A Line in 2D Space.
 * Author: Jakob Schaal
 */
public class Line2 {
	public Vector2Double start;
	public Vector2Double end;
	public Vector2Double dirNormalized{
		get{
			return (end - start).normalized;
		}
	}
	public Vector2Double dir{
		get{
			return end - start;
		}
	}

	public bool completlyInitialized = true;

	public Line2(Vector2Double start, Vector2Double end){
		this.start = start;
		this.end = end;
	}
	public Line2(Vector2Double start, List<Line2> voronoiEdges){
		this.start = start;
		completlyInitialized = false;
		voronoiEdges.Add(this);
	}

	public void finish(Vector2Double end){
		if (completlyInitialized) {
			throw new UnityException("Line is already finished!");
		}
		this.end = end;
		completlyInitialized = true;

		Vector2Double realStart = start;

		/*if (start.x < 0) {
			start.y += dirNormalized.y * (-start.x / dirNormalized.x);
			start.x = 0;
		}
		if (this.end.x < 0) {
			this.end.y -= dirNormalized.y * (-this.end.x / dirNormalized.x);
			this.end.x = 0;
		}
		if (start.y < 0)
			start.y = 0;
		if (this.end.y < 0)
			this.end.y = 0;

		if (start.x > VoronoiFieldCalculatorScript.size) {
			start.x = VoronoiFieldCalculatorScript.size;
		}
		if (this.end.x > VoronoiFieldCalculatorScript.size) {
			this.end.x = VoronoiFieldCalculatorScript.size;
		}
		if (start.y > VoronoiFieldCalculatorScript.size) {
			start.y = VoronoiFieldCalculatorScript.size;
		}
		if (this.end.y > VoronoiFieldCalculatorScript.size) {
			this.end.y = VoronoiFieldCalculatorScript.size;
		}*/
	}

	public double length(){
		if (!completlyInitialized)
			throw new UnityException ("Line2 is not completly Initialized!");
		return (start - end).magnitude;
	}

	public double lengthSqr(){
		if (!completlyInitialized)
		throw new UnityException ("Line2 is not completly Initialized!");
		return (start - end).sqrMagnitude;
	}

	public bool shorterThan(Line2 other){
		if (!completlyInitialized)
			throw new UnityException ("Line2 is not completly Initialized!");
		return this.lengthSqr() < other.lengthSqr();
	}

	public bool longerThan(Line2 other){
		if (!completlyInitialized)
			throw new UnityException ("Line2 is not completly Initialized!");
		return this.lengthSqr () > other.lengthSqr();
	}

	public static int compareMax(Line2 first, Line2 second){
		if (!first.completlyInitialized)
			throw new UnityException ("first is not completly Initialized!");
		if (!second.completlyInitialized)
			throw new UnityException ("second is not completly Initialized!");

		double length1 = first.lengthSqr();
		double length2 = second.lengthSqr ();

		if (length2 > length1) {
			return 1;
		} else if (length1 > length2) {
			return -1;
		} else {
			return 0;
		}
	}
	/*
	 * Siehe: http://en.wikipedia.org/wiki/Line%E2%80%93line_intersection
	 */
	public Vector2Double intersect(Line2 other){
		double A = this.start.x * this.end.y - this.start.y * this.end.x;
		double B = other.start.x - other.end.x;
		double C = this.start.x - this.end.x;
		double D = other.start.x * other.end.y - other.start.x * other.end.x;
		double E = other.start.y - other.end.y;
		double F = this.start.y - this.end.y;
		double G = other.start.x - other.end.x;


		double x = (A*B - C*D)/(C*E - F*G);
		double y = (A*E - F*D)/(C*E - F*G);

		return new Vector2Double(x, y);
	}

}
