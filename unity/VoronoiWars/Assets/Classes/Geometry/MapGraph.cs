﻿using UnityEngine;
using System.Collections.Generic;

/*
 * MapGraph is used to store the complete graph of the voronoi map in a list of nodes
 * provides methods to:
 * 	add a node
 * 	get the complete graph
 * 	get a node by index
 * 	get a node by coordinates
 * Author: Bernhard Maier
 */

public class MapGraph {

	private List<VoronoiField> graph;
	private int amountOfNodes;

	//default constructor
	public MapGraph()
	{
		graph = new List<VoronoiField>();
	}

	//add a node to the list
	public void addNode(VoronoiField vf)
	{
		graph.Add(vf);
		++amountOfNodes;
	}

	//get complete graph
	public List<VoronoiField> getGraph()
	{
		return graph;
	}

	//get node by index
	public VoronoiField getNode(int index)
	{
		return graph[index];
	}
	
	//get node by coordinates
	public VoronoiField getNode(Vector2Double pos)
	{
		for(int i=0; i<amountOfNodes; ++i)
		{
			if(graph[i].calculateCentroid()==pos)
			{
				return graph[i];
			}
		}
		return null;
	}

	//get amount of nodes
	public int getAmountOfNodes()
	{
		return amountOfNodes;
	}
}
