﻿using UnityEngine;
using System.Collections;

public class LoadingScreen : MonoBehaviour {

	public string SceneToLoad;
	private int frame = 0;
	public static LoadingScreen instance;

	// Use this for initialization
	void Start () {
		if (instance != null) {
			throw new UnityException("Singleton violation!");
		}
		instance = this;
	
	}
	
	// Update is called once per frame
	void Update () {
		frame++;
		if (frame == 5) {
				Application.LoadLevel (SceneToLoad);
		}
	}
}
