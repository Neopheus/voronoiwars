﻿using UnityEngine;
using System.Collections;

public class InputBlocker : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (NetworkManager.instance.networkMode != NetworkManager.NetworkMode.hotSeat) {
			if (GameSupervisor.instance.faction != GameSupervisor.instance.ownFaction) {
				this.transform.localScale = new Vector3 (1, 1, 1);
			} else {
				this.transform.localScale = new Vector3 (0, 0, 0);
			}
		} else {
			this.transform.localScale = new Vector3 (0, 0, 0);
		}
	}
}
