﻿using UnityEngine;
using System.Collections;

/**
 * 
 * Author: Jakob Schaal
 */
public class VideoCameraMover : MonoBehaviour {
	public Transform lookat;
	public Transform start;
	public Transform stop;

	public float startFoV;
	public float stopFoV;

	public float div = 10;

	private float t;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void LateUpdate () {
		t += Time.deltaTime / div;

		Vector3 newPos = Vector3.zero;
		newPos.x = Interpolations.interpolCos (start.position.x, stop.position.x, t);
		newPos.y = Interpolations.interpolCos (start.position.y, stop.position.y, t);
		newPos.z = Interpolations.interpolCos (start.position.z, stop.position.z, t);

		this.transform.GetComponent<Camera> ().fieldOfView = Interpolations.interpolCos (startFoV, stopFoV, t);

		this.transform.position = newPos;
		this.transform.LookAt (lookat.position);
	}
}
