using UnityEngine;
using System.Collections;
using UnityEngine.UI;

/* 	
 * Author: Jakob Schaal
 */
public class GameSupervisor : MonoBehaviour {
	public static GameSupervisor instance;
	public GameObject camera;
	public GameObject tank;
	public GameObject antiAir;
	public GameObject heli;
	public GameObject SupplyCrate;
	public Image hexGrid;
	public Factions winningPlayer = Factions.none;
	public Factions ownFaction = Factions.none;
	public Button nextTurnButton;

	private Factions faction_ = Factions.none;
	private int _roundNumber = 0;
	private int _resourceRed;
	private int _resourceBlue;

	private Color RED = new Color (1, 0, 0, 0.5f);
	private Color BLUE = new Color (0, 0, 1, 0.5f);

	private Color startColor;
	private Vector3 startCamPos;
	private Quaternion startCamRot;
	private float transitionValue;

	private Color targetColor;
	private Vector3 targetCamPos;
	private Quaternion targetCamRot;

	public bool nukeFired = false;

	public Factions faction{
		get{
			return faction_;
		}
	}

	public int roundNumber{
		get{
			return _roundNumber;
		}
	}
	public int resourceRed{
		get{
			return _resourceRed;
		}
		set{
			_resourceRed = value;
		}
	}
	public int resourceBlue{
		get{
			return _resourceBlue;
		}
		set{
			_resourceBlue = value;
		}
	}
	public int resourceCurrent{
		get{
			if(faction_ == Factions.blue){
				return _resourceBlue;
			}
			else if(faction_ == Factions.red){
				return _resourceRed;
			}
			else{
				return -1;
			}
		}
		set{
			if(faction_ == Factions.blue){
				_resourceBlue = value;
			}
			else if(faction_ == Factions.red){
				_resourceRed = value;
			}
		}
	}


	public VoronoiField selectedField;


	public Vector3[] camPos = new Vector3[2];
	public Vector3[] camRot = new Vector3[2];

	// Use this for initialization
	void Start () {
		if (instance != null) {
			throw new UnityException("Singleton violation!");
		}
		instance = this;

		newMatch();

		if (NetworkManager.instance.networkMode != NetworkManager.NetworkMode.hotSeat) {
			
			targetCamPos = camPos [(int)ownFaction];
			targetCamRot = Quaternion.Euler (camRot[(int)ownFaction]);

			if(ownFaction == Factions.red){
				targetColor = Color.red;
			} else if(ownFaction == Factions.blue){
				targetColor = Color.blue;
			} else {
				throw new UnityException("Illegal Faction! Faction was " + ownFaction);
			}
		}
	}

	public void newMatch(){
		newRound ();
		winningPlayer = Factions.none;
		_resourceRed = Settings.STARTMONEY;
		_resourceBlue = Settings.STARTMONEY;

		// set the factions of the networkgame according to the seed
		if(NetworkManager.instance.networkMode == NetworkManager.NetworkMode.isHost){
			if(Mathf.Abs(UnityEngine.Random.seed) % 2 == 0) ownFaction=Factions.blue;
			if(Mathf.Abs(UnityEngine.Random.seed) % 2 == 1) ownFaction=Factions.red;
		}
		if(NetworkManager.instance.networkMode == NetworkManager.NetworkMode.isClient){
			if(Mathf.Abs(UnityEngine.Random.seed) % 2 == 0) ownFaction=Factions.red;
			if(Mathf.Abs(UnityEngine.Random.seed) % 2 == 1) ownFaction=Factions.blue;
		}
		Debug.Log ("CurrentFaction: " + faction + " | OwnFaction: " + ownFaction + " Seed: " + UnityEngine.Random.seed);
	}


	// Update is called once per frame
	void Update () {
		if (GameSupervisor.instance == null) {
			instance = this; //EIGENTLICH sollte das nie passieren, aber in Unity gibt es einen nervigen bug wenn man aus dem programm tabbt und wieder zurück.
		}

		if (GameSupervisor.instance.selectedField != null) {
			Vector3 field = Camera.main.WorldToScreenPoint (GameSupervisor.instance.selectedField.worldPos);
			Vector3 mouse = Input.mousePosition;

			field.z = 0;
			mouse.z = 0;


			if (Vector3.Distance (mouse, field) > 120) {
				GameSupervisor.instance.selectedField = null;
			}
		}

		if (faction_ == Factions.none) {
			GameSupervisor.instance.selectedField = null;
		}

		if (Input.GetKeyDown (KeyCode.F1)) {
			//NewMap ();
		}

		nextTurnButton.interactable = !UnitsMoving () && !nukeFired;
	}

	public bool UnitsMoving(){
		{
			GameObject[] gos = GameObject.FindGameObjectsWithTag("UnitRed");
			for(int i = 0; i<gos.Length; i++){
				UnitState state = gos[i].GetComponent<Unit>().state;
				if(state != UnitState.ready && state != UnitState.exhausted && state != UnitState.spawning){
					return true;
				}
			}
		}
		{
			GameObject[] gos = GameObject.FindGameObjectsWithTag("UnitBlue");
			for(int i = 0; i<gos.Length; i++){
				UnitState state = gos[i].GetComponent<Unit>().state;
				if(state != UnitState.ready && state != UnitState.exhausted && state != UnitState.spawning){
					return true;
				}
			}
		}
		return false;
	}

	void LateUpdate(){
		if (transitionValue >= 1)
			return;
		transitionValue += Time.deltaTime;
		transitionValue = Mathf.Clamp01 (transitionValue);
		hexGrid.color = new Color (
			Interpolations.interpolCos(startColor.r, targetColor.r, transitionValue),
			Interpolations.interpolCos(startColor.g, targetColor.g, transitionValue),
			Interpolations.interpolCos(startColor.b, targetColor.b, transitionValue),
			Interpolations.interpolCos(startColor.a, targetColor.a, transitionValue)
			);

		camera.transform.position = new Vector3 (
			Interpolations.interpolLinear(startCamPos.x, targetCamPos.x, transitionValue),
			Interpolations.interpolLinear(startCamPos.y, targetCamPos.y, transitionValue),
			Interpolations.interpolLinear(startCamPos.z, targetCamPos.z, transitionValue)
			);

		camera.transform.rotation = Quaternion.Slerp (startCamRot, targetCamRot, transitionValue);
	}

	public void NewMap(){
		PumpJackScript.pumpJacks.Clear ();
		Application.LoadLevel ("Dashboard");
	}

	public void Win(Factions faction){
		print ("Winning " + faction);
		winningPlayer = faction;
		MatchEndScript.instance.showMatchEndScreen (faction);
	}

	public void newRound(){
		swapPlayer ();
		setAllUnitsReady ();
		if(NetworkManager.instance.networkMode == NetworkManager.NetworkMode.hotSeat){
			setCameraPos ();
			setCameraRot ();
			startTransition ();
		}
		Unit.selected = null;
		selectedField = null;
		VoronoiFieldCalculatorScript.instance.turnOffAllLights ();
		addResource ();
		spawnCrate ();
	}

	private void spawnCrate(){
		VoronoiField vf = VoronoiFieldCalculatorScript.instance.getRandomEmptyNonPlayerField ();
		if (vf != null) {
			GameObject go = (GameObject)GameObject.Instantiate(SupplyCrate);
			go.GetComponent<SupplyCrate>().ini (vf);
		}
	}

	private void addResource(){
		int pumpJacks = 0;
		for (int i = 0; i<PumpJackScript.pumpJacks.Count; i++) {
			if(PumpJackScript.pumpJacks[i].faction == this.faction){
				pumpJacks++;
				PumpJackScript.pumpJacks[i].ShowIncome();
			}
		}
		resourceCurrent += (int)ResourceCalculations.income_formula (pumpJacks, roundNumber / 2, 0);
	}

	public int calculateIncome(){
		int pumpJacks = 0;
		for (int i = 0; i<PumpJackScript.pumpJacks.Count; i++) {
			if(PumpJackScript.pumpJacks[i].faction == this.faction){
				pumpJacks++;
			}
		}
		return (int)ResourceCalculations.income_formula (pumpJacks, (roundNumber / 2) + 1, 0); 
	}

	private void startTransition(){
		transitionValue = 0;
		startCamPos = camera.transform.position;
		startCamRot = camera.transform.rotation;
		startColor = hexGrid.color;

		targetColor = faction == Factions.red ? RED : BLUE;
	}

	private void swapPlayer(){
		if (faction_ == Factions.none) {
			if (Random.Range (0f, 1f) > 0.5f) {
				faction_ = Factions.red;
			} else {
				faction_ = Factions.blue;
			}
		} else if (faction_ == Factions.blue) {
			faction_ = Factions.red;
		} else if (faction_ == Factions.red) {
			faction_ = Factions.blue;
		} else {
			throw new UnityException("Illegal Faction! " + faction_.ToString());
		}

		_roundNumber++;
	}

	private void setAllUnitsReady(){
		if (faction_ == Factions.blue) {
			GameObject[] gos = GameObject.FindGameObjectsWithTag("UnitBlue");
			foreach(GameObject go in gos){
				go.GetComponent<Unit>().state = UnitState.ready;
			}
		} else {
			GameObject[] gos = GameObject.FindGameObjectsWithTag("UnitRed");
			foreach(GameObject go in gos){
				go.GetComponent<Unit>().state = UnitState.ready;
			}
		}
	}

	private void setCameraPos(){
		targetCamPos = camPos [(int)faction_];
	}
	private void setCameraRot(){
		targetCamRot = Quaternion.Euler (camRot[(int)faction_]);
	}

	public void buyTank(){
		GameObject go = (GameObject)GameObject.Instantiate (tank, selectedField.worldPos, Quaternion.identity);
		go.GetComponent<Unit> ().ini (UnitType.tank, GameSupervisor.instance.selectedField);
		GameSupervisor.instance.selectedField = null;
	}

	public void buyAntiAir(){
		GameObject go = (GameObject)GameObject.Instantiate (antiAir, selectedField.worldPos, Quaternion.identity);
		go.GetComponent<Unit> ().ini (UnitType.antiAir, GameSupervisor.instance.selectedField);
		GameSupervisor.instance.selectedField = null;
	}

	public void buyHeli(){
		GameObject go = (GameObject)GameObject.Instantiate (heli, selectedField.worldPos, Quaternion.identity);
		go.GetComponent<Unit> ().ini (UnitType.heli, GameSupervisor.instance.selectedField);
		GameSupervisor.instance.selectedField = null;
	}

	//RPCs

	public void buyTankRPC(int fieldID){
		GetComponent<NetworkView> ().RPC ("_RPC_buyTankRPC", RPCMode.All, fieldID);
	}
	[RPC]
	void _RPC_buyTankRPC(int fieldID){
		GameSupervisor.instance.selectedField = VoronoiFieldCalculatorScript.instance.getFieldById(fieldID);
		GameSupervisor.instance.buyTank ();
	}
	
	public void buyAntiAirRPC(int fieldID){
		GetComponent<NetworkView> ().RPC ("_RPC_buyAntiAirRPC", RPCMode.All, fieldID);
	}
	[RPC]
	void _RPC_buyAntiAirRPC(int fieldID){
		GameSupervisor.instance.selectedField = VoronoiFieldCalculatorScript.instance.getFieldById(fieldID);
		GameSupervisor.instance.buyAntiAir ();
	}
	
	public void buyHeliRPC(int fieldID){
		GetComponent<NetworkView> ().RPC ("_RPC_buyHeliRPC", RPCMode.All, fieldID);
	}
	[RPC]
	void _RPC_buyHeliRPC(int fieldID){
		GameSupervisor.instance.selectedField = VoronoiFieldCalculatorScript.instance.getFieldById(fieldID);
		GameSupervisor.instance.buyHeli ();
	}
	
	public void newRoundRPC(){
		GetComponent<NetworkView> ().RPC ("_RPC_newRoundRPC", RPCMode.All);
	}
	[RPC]
	void _RPC_newRoundRPC(){
		GameSupervisor.instance.newRound ();
	}
	



}
