﻿using UnityEngine;
using System.Collections;
//Kann später weg (falls ichs vergesse)
/* 	
 * Author: Jakob Schaal
 */
public class FoVTestJakobScript : MonoBehaviour {
	private Camera cam;

	// Use this for initialization
	void Start () {
		cam = this.transform.GetComponent<Camera> ();
	}
	
	// Update is called once per frame
	void Update () {
		cam.fieldOfView += Time.deltaTime;
	}
}
