﻿using UnityEngine;
using System.Collections;

public class UnitReadyIndicator : MonoBehaviour {
	private Vector3 startScale;
	private Quaternion startRot;
	private Unit unit;

	private GameObject dot0;
	private GameObject dot1;

	private float scale;
	private float rotation0;
	private float rotation1;

	// Use this for initialization
	void Start () {
		dot0 = this.transform.GetChild (0).gameObject;
		dot1 = this.transform.GetChild (1).gameObject;
		startScale = dot0.transform.localScale;
		startRot = this.transform.rotation;
		unit = this.transform.parent.GetComponent<Unit> ();
		dot0.transform.localScale = Vector3.zero;
		dot1.transform.localScale = Vector3.zero;

		Factions faction = unit.faction;
		Color color = new Color(0, 0, 0);
		if (faction == Factions.blue) {
			color = new Color (0, 0, 1, 0.5f);
		} else {
			color = new Color (1, 0, 0, 0.5f);
		}
		this.GetComponent<SpriteRenderer> ().color = color;
		dot0.GetComponent<SpriteRenderer> ().color = color;
		dot1.GetComponent<SpriteRenderer> ().color = color;
	}
	
	// Update is called once per frame
	void Update () {
		if (unit.state == UnitState.ready && unit.faction == GameSupervisor.instance.faction) {
			scale += Time.deltaTime;
		} else {
			scale -= Time.deltaTime * 3;
		}

		scale = Mathf.Clamp01 (scale);
		rotation0 = (rotation0 + Time.deltaTime * 90) % 360;
		rotation1 = (rotation1 - Time.deltaTime * 50);
		if (rotation1 < 0)
			rotation1 += 360;

		dot0.transform.localScale = startScale * scale;
		dot0.transform.rotation = Quaternion.Euler (new Vector3 (startRot.eulerAngles.x, rotation0, 0));
		dot1.transform.localScale = startScale * scale;
		dot1.transform.rotation = Quaternion.Euler (new Vector3 (startRot.eulerAngles.x, rotation1, 0));
	}
}
