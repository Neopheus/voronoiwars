﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


/* 	
 * Author: Jakob Schaal
 */
public class Unit : MonoBehaviour {
	public static Unit selected{
		get{
			return selected_;
		}
		set{
			VoronoiFieldCalculatorScript.instance.turnOffAllLights();
			if(value != null){
				GameSupervisor.instance.selectedField = null;
				if(value.state == UnitState.ready){
					VoronoiFieldCalculatorScript.instance.Click(value.vf);
				}
			}

			if(value != null && selected_ != value){
				value.GetComponent<AudioSource>().PlayOneShot(value.selectSound);
			}
			
			selected_ = value;
		}
	}
	private static Unit selected_ = null;

	
	public AudioClip selectSound;
	public AudioClip moveSound;
	public AudioClip attackSound;

	public GameObject[] meshesRed = new GameObject[4];
	public GameObject[] meshesBlue = new GameObject[4];

	public GameObject textNode;
	public GameObject explosion;
	
	public int maxHP = 100;
	public const int atk_ = 1;
	
	private bool iniDone = false;
	private bool attacker = false;
	public int hp_;
	private int shownHp_;
	private int hpInterpolStart_;
	
	private float timeSinceHpChange;
	
	public UnitState state = UnitState.spawning;
	public Factions faction = GameSupervisor.instance.faction;
	public UnitType unitType;
	
	public VoronoiField vf;
	private Vector3 startPosition;
	private Quaternion startRotation;
	private Vector3 startScale;

	public List<VoronoiField> neighbors;
	private List<VoronoiField> path;
	private Vector3 forwardStart;
	
	private float movePercentage = 0;
	
	private GameObject attackTarget;
	
	private float timeSinceDeath = 0;
	
	private float timeSinceSpawn = 0;
	
	private AnimationStateController animationStateController;
	
	public int hp{
		get{
			return hp_;
		}
		set{
			timeSinceHpChange = 0;
			hpInterpolStart_ = shownHp_;
			hp_ = value;
		}
	}
	
	public int shownHp{
		get{
			return shownHp_;
		}
	}
	
	public int atk {
		get {
			return atk_;
		}
	}
	
	public void ini(UnitType ut, VoronoiField vf){
		if (ut == null) {
			throw new UnityException("ut mustn't be null");
		}
		
		if (vf == null) {
			throw new UnityException("vf mustn't be null");
		}
		
		unitType = ut;
		this.vf = vf;
		vf.unit = this;
		iniDone = true;
	}
	
	// Use this for initialization
	void Start () {
		if (!iniDone) {
			Debug.LogError("ACHTUNG! ini wurde nicht ausgeführt, daten sind eventuell dirty!");
		}
		
		if (unitType != UnitType.hq) {
			faction = GameSupervisor.instance.faction;
		}
		GameObject[] meshes = null;
		if (faction == Factions.blue) {
			meshes = meshesBlue;
		}
		else {
			meshes = meshesRed;
		}
		
		
		if (unitType == UnitType.hq) {
			maxHP = Settings.UNIT_HP_HQ;
		}
		
		hp_ = maxHP;
		shownHp_ = maxHP;
		hpInterpolStart_ = maxHP;
		
		GameObject go = (GameObject)GameObject.Instantiate (meshes[(int)unitType], this.transform.position, Quaternion.identity);
		go.transform.parent = this.transform;
		if (unitType == UnitType.heli) {
			go.transform.localPosition = new Vector3(0, 4, 0);
		}
		
		if (faction == Factions.blue) {
			this.gameObject.tag = "UnitBlue";
		}
		if (faction == Factions.red) {
			this.gameObject.tag = "UnitRed";
		}
		
		timeSinceSpawn += Time.deltaTime;
		
		string nameFaction = "";
		string nameType = "";
		if (faction == Factions.blue) {
			nameFaction = "Blue";
		} else {
			nameFaction = "Red";
		}
		
		if (unitType == UnitType.antiAir) {
			nameType = "AntiAir";
		}else if (unitType == UnitType.tank) {
			nameType = "Tank";
		}else if (unitType == UnitType.heli) {
			nameType = "Heli";
		}else if (unitType == UnitType.hq) {
			nameType = "HQ";
		}
		
		this.gameObject.name = nameFaction + " " + nameType;
		state = UnitState.spawning;
		animationStateController = this.transform.GetChild (2).GetChild (0).GetComponent<AnimationStateController> ();

		if (faction == Factions.blue) {
			VoronoiFieldCalculatorScript.instance.LookAtHq(this.gameObject, Factions.red);
		}
		if (faction == Factions.red) {
			VoronoiFieldCalculatorScript.instance.LookAtHq(this.gameObject, Factions.blue);
		}
		startPosition = this.transform.position;
		startRotation = this.transform.rotation;
		startScale = this.transform.localScale;
	}
	
	// Update is called once per frame
	void Update () {
//		if(unitType == UnitType.hq){
//			print (this.name + " | " + this.transform.position);
//		}
		timeSinceHpChange += Time.deltaTime;

		if (unitType == UnitType.hq && state != UnitState.dying && state != UnitState.fighting) {
			if(state == UnitState.ready){
				GameObject go = (GameObject)GameObject.Instantiate(textNode, this.transform.position, Quaternion.identity);
				go.transform.GetChild(0).GetComponent<TextMesh>().text = "+ "+Settings.CURRENCY+Settings.BASE_INCOME;
			}
			state = UnitState.exhausted;
		}
		
		if (timeSinceHpChange < 3) {
			shownHp_ = (int)Interpolations.interpolCos (hpInterpolStart_, hp_, timeSinceHpChange / 3f);
		} else {
			shownHp_ = hp_;
		}
		
		if (state != UnitState.ready && selected == this) {
			selected = null;
		}

		
		timeSinceSpawn += Time.deltaTime;

		if (selected == this && state == UnitState.ready) {
			VoronoiFieldCalculatorScript.instance.Click(vf);
		}

		switch (state) {
		case UnitState.spawning:
			SpawningUpdate();
			break;
		case UnitState.exhausted:
			ExhaustedUpdate();
			break;
		case UnitState.fighting:
			FightingUpdate();
			break;
		case UnitState.moving:
			MovingUpdate();
			break;
		case UnitState.ready:
			ReadyUpdate();
			break;
		case UnitState.dying:
			DyingUpdate();
			break;
		default:
			throw new UnityException("Illegal State! State was "+ state);
		}
		
		if ((vf == null || vf.unit != this) && state != UnitState.dying) {
			throw new UnityException("Illegal VoronoiField - Unit state!");
		}
		
		checkAnimation ();

		if (shownHp <= 0) {
			state = UnitState.dying;
		}
	}

	void LateUpdate(){
		if (this.unitType == UnitType.hq) {
			this.transform.position = startPosition;
			this.transform.rotation = startRotation;

			if(state != UnitState.dying) {
				this.transform.localScale = startScale;
			} 
		}
	}
	
	private void checkAnimation(){
		if (animationStateController != null) {
			switch(state){
			case(UnitState.dying):
				animationStateController.myCurrentAnimationState = AnimationStateController.AnimationState.death;
				break;
			case(UnitState.exhausted):
				animationStateController.myCurrentAnimationState = AnimationStateController.AnimationState.idle;
				break;
			case(UnitState.fighting):
				animationStateController.myCurrentAnimationState = AnimationStateController.AnimationState.fight;
				break;
			case(UnitState.moving):
				animationStateController.myCurrentAnimationState = AnimationStateController.AnimationState.move;
				break;
			case(UnitState.ready):
				animationStateController.myCurrentAnimationState = AnimationStateController.AnimationState.idle;
				break;
			case(UnitState.spawning):
				animationStateController.myCurrentAnimationState = AnimationStateController.AnimationState.spawn;
				break;
			default:
				throw new UnityException("Illegal State!");
			}
		}
	}
	
	public void Attack(VoronoiField vf){
		if (vf.unit == null) {
			throw new UnityException("The other field had no Unit to attack!");
		}
		
		BattleCalculation.calculateBattle (this, vf.unit);
		attackTarget = vf.unit.gameObject;
		vf.unit.attackTarget = this.gameObject;
		attacker = true;
		attackTarget.GetComponent<Unit> ().attacker = false;
		
		state = UnitState.fighting;
		vf.unit.state = UnitState.fighting;
		VoronoiFieldCalculatorScript.instance.turnOffAllLights ();

		if (this.unitType != UnitType.hq) {
			this.transform.LookAt (attackTarget.transform.position);
		} else {
			this.transform.GetChild (2).GetChild(0).GetChild(2).LookAt(attackTarget.transform.position);
		}
		if (attackTarget.GetComponent<Unit> ().unitType != UnitType.hq) {
			attackTarget.transform.LookAt (this.transform.position);
		} else {
			attackTarget.transform.GetChild (2).GetChild(0).GetChild(2).LookAt(this.transform.position);
		}
	}
	
	public void MoveTo(VoronoiField vf, bool turnOffLights = true){
		if (state != UnitState.ready && state != UnitState.fighting) {
			throw new UnityException("Illegal State!");
		}
		
		state = UnitState.moving;
		movePercentage = 0;
		path = Pathfinding.calculatePath(neighbors, this.vf, vf);
		SupplyCrate.notifyMovement (path, faction);
		this.vf.unit = null;
		this.vf = vf;
		this.vf.unit = this;
		forwardStart = this.transform.forward;
		if(turnOffLights) VoronoiFieldCalculatorScript.instance.turnOffAllLights ();
		if (selected == this)
			selected = null;
	}
	
	public void OnMouseDown(){
		if (state == UnitState.ready && GameSupervisor.instance.faction == this.faction) {
			if(Unit.selected != this){
				Unit.selected = this;
				neighbors = VoronoiFieldCalculatorScript.instance.Click(vf);
			}
			else{
				Unit.selected = null;
				VoronoiFieldCalculatorScript.instance.turnOffAllLights();
			}
		}
	}
	
	private void SpawningUpdate(){
		if (timeSinceSpawn > 2) {
			state = UnitState.exhausted;
		}
	}
	
	private void ReadyUpdate(){
		this.transform.position = vf.worldPos;
	}
	
	private void FightingUpdate(){
		if (shownHp <= 0) {
			attackTarget.GetComponent<Unit>().state = UnitState.exhausted;
			state = UnitState.dying;
			print ("After fight outer " + attacker);
			if(attacker == false){
				print ("After fight");
				attackTarget.GetComponent<Unit>().state = UnitState.fighting;
				attackTarget.GetComponent<Unit>().MoveTo(vf, false);
				vf = null;
			}
		}
	}
	
	private void ExhaustedUpdate(){
		this.transform.position = vf.worldPos;
	}
	
	private void DyingUpdate(){
		if (unitType == UnitType.hq && GameSupervisor.instance.winningPlayer == Factions.none) {
			if (faction == Factions.blue) {
				GameSupervisor.instance.Win(Factions.red);
			}
			if (faction == Factions.red) {
				GameSupervisor.instance.Win (Factions.blue);
			}
		}

		float timeSinceDeathLast = timeSinceDeath;
		timeSinceDeath += Time.deltaTime;
		
		if (timeSinceDeath > 2) {
			if(timeSinceDeathLast <= 2){
				GameObject go = (GameObject)GameObject.Instantiate(explosion, this.transform.position, Quaternion.identity);
			}
			this.transform.localScale = new Vector3(3 - timeSinceDeath, 3 - timeSinceDeath, 3 - timeSinceDeath);
			if(timeSinceDeath > 3){
				
				GameObject.Destroy(this.gameObject);
			}
		}
		this.transform.GetChild(0).localScale = Vector3.zero;
	}
	
	private void MovingUpdate(){
		UnitMover.moveUnit (path, forwardStart, this.transform, movePercentage);
		movePercentage += Time.deltaTime / path.Count;

		if (movePercentage > 1) {
			state = UnitState.exhausted;
			this.transform.position = vf.worldPos;
			Vector3 forward = this.transform.forward;
			forward.y = 0;
			forward.Normalize();
			this.transform.LookAt(this.transform.position + forward);
		}
	}

	public void MoveToRPC(int fieldIDsrc, int fieldIDdest, bool lights){
		GetComponent<NetworkView> ().RPC ("_RPC_MoveToRPC", RPCMode.All, fieldIDsrc, fieldIDdest, lights);
	}
	[RPC]
	void _RPC_MoveToRPC(int fieldIDsrc, int fieldIDdest, bool lights){
		print ("field Src Unit" + fieldIDsrc + " | Unit Name Src VFC" + VoronoiFieldCalculatorScript.instance.getFieldById(fieldIDsrc).unit.name);
		GameSupervisor.instance.selectedField = VoronoiFieldCalculatorScript.instance.getFieldById(fieldIDsrc);
		//VoronoiFieldCalculatorScript.instance.getFieldById(fieldIDsrc).unit.neighbors = RangeDetection.calculateRange(GameSupervisor.instance.selectedField, 3);
		GameSupervisor.instance.selectedField.unit.MoveTo(VoronoiFieldCalculatorScript.instance.getFieldById(fieldIDdest), lights);
	}

	public void AttackRPC(int fieldIDatk, int fieldIDdef, bool lights){
		GetComponent<NetworkView> ().RPC ("_RPC_AttackRPC", RPCMode.All, fieldIDatk, fieldIDdef, lights);
	}
	[RPC]
	void _RPC_AttackRPC(int fieldIDatk, int fieldIDdef, bool lights){
		GameSupervisor.instance.selectedField = VoronoiFieldCalculatorScript.instance.getFieldById (fieldIDatk);
		GameSupervisor.instance.selectedField.unit.Attack(VoronoiFieldCalculatorScript.instance.getFieldById(fieldIDdef));
	}
}


public enum UnitState{
	ready, fighting, exhausted, moving, dying, spawning
}

public enum UnitType{
	antiAir = 0, tank = 1, heli = 2, hq = 3
}
