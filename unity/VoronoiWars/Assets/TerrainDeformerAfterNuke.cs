﻿using UnityEngine;
using System.Collections;
using System.Threading;

public class TerrainDeformerAfterNuke : MonoBehaviour {
	public static TerrainDeformerAfterNuke instance;

	private bool calculationStarted = false;
	private bool calculationEnded = false;

	private float[,] heights;

	private Terrain terrain;
	private TerrainData terrainData;
	private Vector3 pos;

	private Vector2 terrainPos;
	private float[,] oldHeights;
	private float[,] newHeights;

	// Use this for initialization
	void Start () {
		if (instance != null) {
			throw new UnityException("Singleton Violation");
		}
		instance = this;

		terrain = this.GetComponent <Terrain> ();
		terrainData = terrain.terrainData;
	}

	public void explode(Vector3 pos){
		this.pos = pos;
		oldHeights = terrainData.GetHeights (0, 0, terrainData.heightmapWidth, terrainData.heightmapHeight);
		newHeights = new float[oldHeights.GetLength(0), oldHeights.GetLength(1)];
		Thread t = new Thread(new ThreadStart(calculate));
		terrainPos = Vector2.zero;
		terrainPos.x = pos.z / 200f * terrainData.heightmapWidth;
		terrainPos.y = pos.x / 200f * terrainData.heightmapHeight;
		t.Start();
	}

	private void calculate(){
		calculationStarted = true;




		for (int i = 0; i<newHeights.GetLength(0); i++) {
			for(int k = 0; k<newHeights.GetLength(1); k++){
				float x = terrainPos.x - i;
				float y = terrainPos.y - k;
				float dist = x*x + y*y;
				float heightMulti = dist / 16000f;
				heightMulti = Mathf.Clamp01(heightMulti);
				float realMult = (Interpolations.interpolBezier(0, 1, 1.3f, heightMulti));
				newHeights[i, k] = oldHeights[i, k] * realMult;
			}
		}


		heights = newHeights;
		calculationEnded = true;
	}

	// Update is called once per frame
	void Update () {
		/*if (Input.GetMouseButtonDown (1)) {
			Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
			RaycastHit hit;
			if (Physics.Raycast (ray, out hit)) {
				explode(hit.point);
			}
		}*/

		if (calculationEnded == true && calculationStarted == true){
			calculationStarted = false;
			calculationEnded = false;


			terrainData.SetHeights (0, 0, heights);
		}
	}
}
