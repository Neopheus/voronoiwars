﻿using UnityEngine;
using System.Collections;

/* 	
 * Author: Jakob Schaal
 */
public class Settings {
	private Settings(){} //delete constructor


	public const int NUKE_COST = 750;
	public const int UNIT_HP_HQ = 300;
	public const int UNIT_COST = 100;

	//income
	public const int STARTMONEY = 200;
	public const float BASE_INCOME = 40.0f;
	public const float BASE_INCOME_TOWER = 10.0f;
	public const float DYNAMIC_INCOME = 60.0f;
	public const float TOWER_MULTI = 0.65f;
	public const float ROUND_MULTI = 0.1f;
	public const int CRATE_RESOURCE = 50;

	public const string CURRENCY = "$";
}
