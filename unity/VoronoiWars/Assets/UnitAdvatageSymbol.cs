﻿using UnityEngine;
using System.Collections;

/**
 * Author: Jakob Schaal
 */
public class UnitAdvatageSymbol : MonoBehaviour {
	public Sprite advantageSprite;
	public Sprite equalSprite;
	public Sprite disadvantageSprite;

	public Color advantageColor;
	public Color equalColor;
	public Color disadvantageColor;

	private SpriteRenderer sr;
	private Unit unit;

	private float animationTime = 0;

	private Vector3 startLocalPos;
	private float moveRange = 0.1f;

	// Use this for initialization
	void Start () {
		sr = this.GetComponent<SpriteRenderer> ();
		unit = this.transform.parent.parent.GetComponent<Unit> ();
		startLocalPos = this.transform.localPosition;
	}
	
	// Update is called once per frame
	void Update () {
		animationTime += Time.deltaTime * 1.5f;
		if (animationTime > 1) {
			animationTime -= 1;
		}

		if (Unit.selected == null || unit.faction == Unit.selected.faction) {
			sr.sprite = null;
		} else {
			relations r = relations.equal;

			if(unit.unitType == UnitType.hq){
				r = relations.equal;
			}else if(unit.unitType == UnitType.antiAir){
				if(Unit.selected.unitType == UnitType.tank){
					r = relations.advantage;
				}else if(Unit.selected.unitType == UnitType.heli){
					r = relations.disadvantage;
				}
			}else if(unit.unitType == UnitType.tank){
				if(Unit.selected.unitType == UnitType.heli){
					r = relations.advantage;
				}else if(Unit.selected.unitType == UnitType.antiAir){
					r = relations.disadvantage;
				}
			}else if(unit.unitType == UnitType.heli){
				if(Unit.selected.unitType == UnitType.antiAir){
					r = relations.advantage;
				}else if(Unit.selected.unitType == UnitType.tank){
					r = relations.disadvantage;
				}
			}


			switch(r){
			case(relations.advantage):
				sr.color = advantageColor;
				sr.sprite = advantageSprite;
				this.transform.localPosition = startLocalPos + new Vector3(0, Interpolations.interpolCos(-moveRange, moveRange, animationTime), 0);
				break;
			case(relations.disadvantage):
				sr.color = disadvantageColor;
				sr.sprite = disadvantageSprite;
				this.transform.localPosition = startLocalPos + new Vector3(0, Interpolations.interpolCos(moveRange, -moveRange, animationTime), 0);
				break;
			case(relations.equal):
				sr.color = equalColor;
				sr.sprite = equalSprite;
				this.transform.localPosition = startLocalPos;
				break;
			default:
				throw new UnityException("Illegal relation! Relation was " + r);
			}
		}
	}

	private enum relations{
		advantage, disadvantage, equal
	}
}
