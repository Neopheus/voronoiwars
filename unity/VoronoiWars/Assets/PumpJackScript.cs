﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/* 	
 * Author: Jakob Schaal
 */
public class PumpJackScript : MonoBehaviour {
	public static List<PumpJackScript> pumpJacks = new List<PumpJackScript>();
	public Factions faction{
		get{
			return faction_;
		}
		set{
			if(faction_ != value){
				this.transform.GetChild(0).GetComponent<AudioSource>().Play();

				switch(value){
				case(Factions.blue):
					factionIndicator.color = Color.blue;
					break;
				case(Factions.red):
					factionIndicator.color = Color.red;
					break;
				case(Factions.none):
					factionIndicator.color = Color.white;
					break;
				default:
					throw new UnityException("Unsupportet Faction. Faction was " + value);
				}
			}
			faction_ = value;
		}
	}
	private Factions faction_ = Factions.none;
	public VoronoiField field;
	private SpriteRenderer factionIndicator;
	public GameObject textNode;


	// Use this for initialization
	void Start () {
		if (field == null) {
			throw new UnityException("VoronoidField was null!");
		}

		factionIndicator = this.transform.GetChild (1).GetComponent<SpriteRenderer> ();
		this.transform.rotation = Quaternion.Euler (0, Random.Range (0f, 360f), 0);
		this.transform.position = field.worldPos;
		pumpJacks.Add (this);
	}
	
	// Update is called once per frame
	void Update () {
		if(field.unit != null && (field.unit.state == UnitState.exhausted || field.unit.state == UnitState.ready)){
			faction = field.unit.faction;
		}
		if (Debug.isDebugBuild && field.pumpJack != this.gameObject) {
			throw new UnityException("Illegal PumpJack - Field state");
		}
	}

	public void ShowIncome(){
		GameObject go = (GameObject)GameObject.Instantiate(textNode, this.transform.position, Quaternion.identity);
		go.transform.GetChild(0).GetComponent<TextMesh>().text = "+ "+Settings.CURRENCY+(int)(ResourceCalculations.income_formula(1, GameSupervisor.instance.roundNumber / 2, 0) - Settings.BASE_INCOME);
	}
}
