﻿using UnityEngine;
using System.Collections;

public class TimedDestroy : MonoBehaviour {
	public float timeToLive = 20f;

	private float timeAlive = 0;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		timeAlive += Time.deltaTime;
		if (timeAlive >= timeToLive) {
			GameObject.Destroy(this.gameObject);
		}
	}
}
