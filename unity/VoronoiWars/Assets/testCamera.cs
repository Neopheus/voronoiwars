﻿using UnityEngine;
using System.Collections;

public class testCamera : MonoBehaviour {
	
	public float ScrollSpeed = 150;
	
	public float ScrollEdge = -20.00f;
	
	public float PanSpeed = 100;
	
	public Vector2 ZoomRange = new Vector2( -75, 100 );
	
	public float CurrentZoom = 0;
	
	public float ZoomSpeed = 10;
	
	public float ZoomRotation = 1;
	
	public float RotateSpeed = 50;
	
	private Vector3 InitialPosition;
	
	private Vector3 InitialRotation;
	
	void Start () {

		InitialPosition = transform.position;      
		InitialRotation = transform.eulerAngles;
	}
	
	
	void Update () {
		// panning the camera. if/else if structure necessary in case two keys are pressed

//		if ( Input.GetKey("w") || Input.mousePosition.y >= Screen.height * (1 - ScrollEdge) ) {            
//			transform.Translate(Vector3.forward * Time.deltaTime * PanSpeed, Space.Self );             
//		}
//		else if ( Input.GetKey("s") || Input.mousePosition.y <= Screen.height * ScrollEdge ) {         
//			transform.Translate(Vector3.forward * Time.deltaTime * -PanSpeed, Space.Self );            
//		}
//
//		if ( Input.GetKey("d") || Input.mousePosition.x >= Screen.width * (1 - ScrollEdge) ) {             
//				transform.Translate(Vector3.right * Time.deltaTime * PanSpeed, Space.Self );   
//			}
//		else if ( Input.GetKey("a") || Input.mousePosition.x <= Screen.width * ScrollEdge ) {            
//				transform.Translate(Vector3.right * Time.deltaTime * -PanSpeed, Space.Self );              
//			}
//			
//		if ( Input.GetKey("q") ) {
//				transform.Rotate(Vector3.up * Time.deltaTime * -RotateSpeed, Space.World);
//			}
//		else if ( Input.GetKey("e") ) {
//				transform.Rotate(Vector3.up * Time.deltaTime * RotateSpeed, Space.World);
//			}

		if ( Input.GetKey("w") || Input.mousePosition.y >= Screen.height * (1 - ScrollEdge) ) {            
			transform.Translate(Vector3.forward * Time.deltaTime * PanSpeed, Space.Self );             
		}
		else if ( Input.GetKey("s") || Input.mousePosition.y <= Screen.height * ScrollEdge ) {         
			transform.Translate(Vector3.forward * Time.deltaTime * -PanSpeed, Space.Self );            
		}
		
		if ( Input.GetKey("d") || Input.mousePosition.x >= Screen.width * (1 - ScrollEdge) ) {             
			transform.Translate(Vector3.right * Time.deltaTime * PanSpeed, Space.Self );   
		}
		else if ( Input.GetKey("a") || Input.mousePosition.x <= Screen.width * ScrollEdge ) {            
			transform.Translate(Vector3.right * Time.deltaTime * -PanSpeed, Space.Self );              
		}
		
		if ( Input.GetKey("q") ) {
			transform.Rotate(Vector3.up * Time.deltaTime * -RotateSpeed, Space.World);
		}
		else if ( Input.GetKey("e") ) {
			transform.Rotate(Vector3.up * Time.deltaTime * RotateSpeed, Space.World);
		}
		
		// zoom camera in/out within limits
		CurrentZoom -= Input.GetAxis("Mouse ScrollWheel") * Time.deltaTime * 1000 * ZoomSpeed;
		
		CurrentZoom = Mathf.Clamp( CurrentZoom, ZoomRange.x, ZoomRange.y );
		
		transform.position = new Vector3( transform.position.x, transform.position.y - (transform.position.y - (InitialPosition.y + CurrentZoom)) * 0.1f, transform.position.z );
	}
}
