﻿using UnityEngine;
using System.Collections;

public class NukeWatcher : MonoBehaviour {
	public static NukeWatcher instance;
	private GameObject nuke;
	private Vector3 lastKnownPos;

	private bool started = false;
	private float timeSinceExplode = 0;
	private float timeSinceNukeStart = 0;

	private float startFoV;
	private float targetFoV = 10;

	// Use this for initialization
	void Start () {
		if (instance != null) {
			throw new UnityException("Singleton Violation!");
		}
		instance = this;
	}

	public void watch(GameObject nuke){
		this.nuke = nuke;
		started = true;
		startFoV = this.GetComponent<Camera> ().fieldOfView;
	}
	
	// Update is called once per frame
	void Update () {
		if (started) {
			timeSinceNukeStart += Time.deltaTime;
			if (nuke != null) {
				lastKnownPos = nuke.transform.position;
			}else{
				timeSinceExplode += Time.deltaTime * 2.5f;
				this.transform.position += Random.insideUnitSphere * Mathf.Clamp(5 - timeSinceExplode, 0f, float.MaxValue);
			}

			Quaternion oldRot = this.transform.rotation;
			this.transform.LookAt(lastKnownPos);
			Quaternion newRot = this.transform.rotation;
			this.transform.rotation = Quaternion.Slerp(oldRot, newRot, Time.deltaTime);


			if(timeSinceNukeStart < Mathf.PI*2){
				float fovDiff = targetFoV - startFoV;
				this.GetComponent<Camera>().fieldOfView = startFoV + fovDiff * ((-Mathf.Cos (timeSinceNukeStart) + 1)/2);
			}

		}

	}
}
