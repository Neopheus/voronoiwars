﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SupplyCrate : MonoBehaviour {

	private Transform parachute;

	private VoronoiField vf;
	private bool iniDone = false;

	private float timeAlive = 0;
	public GameObject textNode;

	public void ini(VoronoiField vf){
		this.vf = vf;
		iniDone = true;

		vf.SupplyCrate = this.gameObject;
	}

	// Use this for initialization
	void Start () {
		if (!iniDone) {
			throw new UnityException("Call ini first!");
		}
		parachute = this.transform.GetChild (0).GetChild (0);
		calcPos ();
		this.transform.rotation = Quaternion.Euler (new Vector3 (0, Random.Range (0f, 360f), 0));
	}
	
	// Update is called once per frame
	void Update () {
		timeAlive += Time.deltaTime;
		calcPos ();

		if (vf.unit != null && (vf.unit.state == UnitState.exhausted || vf.unit.state == UnitState.ready)) {
			if(vf.unit.faction == Factions.blue){
				GameSupervisor.instance.resourceBlue += Settings.CRATE_RESOURCE;
			}
			else if(vf.unit.faction == Factions.red){
				GameSupervisor.instance.resourceRed += Settings.CRATE_RESOURCE;
			}
			GameObject.Destroy (this.gameObject);
			vf.SupplyCrate = null;
			GameObject go = (GameObject)GameObject.Instantiate(textNode, this.transform.position, Quaternion.identity);
			go.transform.GetChild(0).GetComponent<TextMesh>().text = "+ "+Settings.CURRENCY+Settings.CRATE_RESOURCE;
		}
	}

	public static void notifyMovement(List<VoronoiField> vfs, Factions faction){
		GameObject[] crates = GameObject.FindGameObjectsWithTag ("Crate");

		for (int i = 0; i<crates.Length; i++) {
			for(int k = 0; k<vfs.Count; k++){
				if(crates[i].GetComponent<SupplyCrate>().vf == vfs[k]){
					if(faction == Factions.blue){
						GameSupervisor.instance.resourceBlue += Settings.CRATE_RESOURCE;
					}
					else if(faction == Factions.red){
						GameSupervisor.instance.resourceRed += Settings.CRATE_RESOURCE;
					}
					GameObject.Destroy (crates[i].gameObject);
					vfs[k].SupplyCrate = null;
					GameObject go = (GameObject)GameObject.Instantiate(crates[i].GetComponent<SupplyCrate>().textNode, crates[i].transform.position, Quaternion.identity);
					go.transform.GetChild(0).GetComponent<TextMesh>().text = "+ "+Settings.CURRENCY+Settings.CRATE_RESOURCE;
				}
			}
		}
	}

	private void calcPos(){
		if (timeAlive < 3) {
			Vector3 pos = vf.worldPos;
			pos.y = Interpolations.interpolLinear (vf.worldPos.y + 200, vf.worldPos.y + 30, (timeAlive) / 3);
			this.transform.position = pos;
			parachute.localScale = Vector3.zero;
		} else if (timeAlive < 6) {
			float scale = Interpolations.interpolCos (0, 1, (timeAlive - 3) * 3);
			parachute.localScale = new Vector3 (scale, 1, scale);
			Vector3 pos = vf.worldPos;
			pos.y = Interpolations.interpolLinear (vf.worldPos.y + 30, vf.worldPos.y, (timeAlive - 3) / 3);
			this.transform.position = pos;
		} else if (timeAlive < 7f) {
			float scale = Interpolations.interpolCos (0, 1, (timeAlive - 6));
			parachute.localScale = new Vector3(1, 1-scale, 1);
			this.transform.position = vf.worldPos;
		} else {
			this.transform.position = vf.worldPos;
			parachute.localScale = Vector3.zero;
		}
	}
}
