﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UnitMover {
	//moves the unit according to the given ratio along the path
	public static void moveUnit(List<VoronoiField> path, Vector3 forwardAtStart, Transform unit, float uberRatio){
		if (path.Count > 0) {
			Vector3 start;
			Vector3 target;
			float subRatio;
			List<Vector3> controlPoints = new List<Vector3>();

			if(uberRatio < 0)
				Debug.Log("Ratio is smaller than 0. Clamping to 0.");

			Mathf.Clamp(uberRatio, 0.0F, 1.0F);

			//value need for further calculations
			float formula = uberRatio * ((float)path.Count - 1);
			//the index of the next field in the path not yet reached
			int index_target_field = (int) formula + 1;
			//subRatio is the uberRatio transformed to a ratio between two fields
			subRatio = formula - ((float)index_target_field - 1.0F);

			start = path[index_target_field - 1].worldPos;
			target = path[index_target_field].worldPos;

			//generate control points between fields on the path, for non-linear movement
			controlPoints.Add(forwardAtStart * 5 + path[0].worldPos);;
			for(int i = 1; i < path.Count - 1; i++){
				Vector3 subVector = path[i].worldPos - new Vector3(Interpolations.interpolBezier(path[i-1].worldPos.x, path[i].worldPos.x, controlPoints[i - 1].x, 0.9F),
				                                                   Interpolations.interpolBezier(path[i-1].worldPos.y, path[i].worldPos.y, controlPoints[i - 1].y, 0.9F),
				                                                   Interpolations.interpolBezier(path[i-1].worldPos.z, path[i].worldPos.z, controlPoints[i - 1].z, 0.9F));
				subVector.Normalize();
				controlPoints.Add(subVector * 5 + path[i].worldPos);
			}

			float controlPointX = controlPoints[index_target_field - 1].x;
			float controlPointY = controlPoints[index_target_field - 1].y;
			float controlPointZ = controlPoints[index_target_field - 1].z;


			//positioning and adjusting the unit at new position
			unit.position = new Vector3(Interpolations.interpolBezier(start.x, target.x, controlPointX, subRatio),
			                            Interpolations.interpolBezier(start.y, target.y, controlPointY, subRatio),
			                            Interpolations.interpolBezier(start.z, target.z, controlPointZ, subRatio));


			{
				Quaternion oldRot = unit.rotation;
				Vector3 lookPos = new Vector3(Interpolations.interpolBezier(start.x, target.x, controlPointX, subRatio + 0.01f),
				                              Interpolations.interpolBezier(start.y, target.y, controlPointY, subRatio + 0.01f),
				                              Interpolations.interpolBezier(start.z, target.z, controlPointZ, subRatio + 0.01f));
				unit.LookAt (lookPos);
				Quaternion newRot = unit.rotation;
				unit.rotation = Quaternion.Slerp (oldRot, newRot, Time.deltaTime * 4);
			}



			if(uberRatio >= 0.6f){
				Quaternion oldRot = unit.rotation;
				Vector3 forward = unit.forward;
				forward.y = 0;
				forward.Normalize();
				unit.LookAt(unit.position + forward);
				Quaternion newRot = unit.rotation;
				unit.rotation = Quaternion.Slerp (oldRot, newRot, (uberRatio - 0.6f)*2.5f);
			}


			if (uberRatio >= 1.0F) {
				unit.position = path[path.Count-1].worldPos;
				Vector3 forward = unit.forward;
				forward.y = 0;
				forward.Normalize();
				unit.LookAt(unit.position + forward);
			}
		}
	}
}