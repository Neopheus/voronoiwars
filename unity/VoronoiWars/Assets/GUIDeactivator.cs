﻿using UnityEngine;
using System.Collections;

public class GUIDeactivator : MonoBehaviour {
	public Vector3 moveOffset;

	private Vector3 startPos;
	private Vector3 endPos;
	private Vector3 controlPos;
	private float t;

	// Use this for initialization
	void Start () {
		startPos = this.transform.position;
		endPos = startPos + moveOffset;
		controlPos = startPos - moveOffset / 2;
		t = 0;

	}
	
	// Update is called once per frame
	void Update () {
		if (NetworkManager.instance.networkMode != NetworkManager.NetworkMode.hotSeat) {
			if(GameSupervisor.instance.ownFaction != GameSupervisor.instance.faction){
				t += Time.deltaTime;
			}else{
				t -= Time.deltaTime;
			}

			t = Mathf.Clamp01(t);

			Vector3 pos = Vector3.zero;
			pos.x = Interpolations.interpolBezier(startPos.x, endPos.x, controlPos.x, t);
			pos.y = Interpolations.interpolBezier(startPos.y, endPos.y, controlPos.y, t);
			pos.z = Interpolations.interpolBezier(startPos.z, endPos.z, controlPos.z, t);
			this.transform.position = pos;
		}
	}
}
