﻿using UnityEngine;
using System.Collections;

public class AnimationStateController : MonoBehaviour {
	public enum AnimationState
	{
		idle = 0,
		spawn = 1,
		airdrop = 2,
		move = 3,
		fight = 4,
		death = 5
	}
	
	public AnimationState myCurrentAnimationState;
	Animator animator;
	AnimationState aState;
	int nState;

	
	
	
	// Use this for initialization
	void Start () {
		myCurrentAnimationState = AnimationState.idle;
		animator = this.GetComponent<Animator>();
		nState = 0;
	}
	
	// Update is called once per frame
	void Update () {
//		if (Input.anyKeyDown) {
//			
//			currentAnimationState = currentAnimationState + 1;

		if (nState != (int)this.myCurrentAnimationState) {
			nState = (int)this.myCurrentAnimationState;
			//Debug.Log("nstate: " + nState);
			
			//ac.currentAnimationState = nState;
			animator.SetInteger ("aniEnum", nState);
		}



			
			
//			if(currentAnimationState == AnimationState.end)
//			{
//				currentAnimationState = AnimationState.idle;
//			}
//		}
	}
}
