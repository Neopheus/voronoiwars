﻿using UnityEngine;
using System.Collections;

/* 	
 * Author: Jakob Schaal
 */
public class CameraCopy : MonoBehaviour {

	private Camera parentCam;
	private Camera thisCam;

	// Use this for initialization
	void Start () {
		parentCam = this.transform.parent.GetComponent<Camera> ();
		thisCam = this.transform.GetComponent<Camera> ();
	}
	
	// Update is called once per frame
	void LateUpdate () {
		thisCam.fieldOfView = parentCam.fieldOfView;
		thisCam.nearClipPlane = parentCam.nearClipPlane;
		thisCam.farClipPlane = parentCam.farClipPlane;
	}
}
