﻿using UnityEngine;
using System.Collections;

public class NetworkManager : MonoBehaviour {
	public enum NetworkMode
	{
		isHost,
		isClient,
		hotSeat
	}

	public static NetworkManager instance;
	public NetworkMode networkMode = NetworkMode.hotSeat;

	public bool tryToConnect = false;
	public bool serverStarted = false;
	public bool connected = false;
	public bool sceneChanged = false;

	int port = 23071; 
	public string hostIp = "";
	int maxPlayers = 1;


	// Use this for initialization
	void Start () {
		if (instance != null) {
			throw new UnityException("Singleton violation!");
		}
		instance = this;

		DontDestroyOnLoad(instance);
	}
	// Update is called once per frame
	void Update () {
		if (networkMode != NetworkMode.hotSeat) {
			if (Network.connections.Length > 0) {
				connected = true;
				tryToConnect = false;
				if(networkMode == NetworkMode.isHost){
					setSeed(MMPanelScript.usedSeed);
					if(!sceneChanged)
					{
						ChangeScene("Dashboard");
						sceneChanged = true;
					}
				}
			} else {
				connected = false;
			}
		}
	}

	public void StartServer(string seed){
		MMPanelScript.usedSeed = seed;
		UnityEngine.Random.seed = seed.GetHashCode ();
		hostIp = Network.player.ipAddress;
		Network.InitializeServer (2, port, false);
	}

	public void ConnectToServer(string ip){
		hostIp = ip;
		Network.Connect (hostIp, port);
	}

	public void ChangeScene(string scene){
		GetComponent<NetworkView> ().RPC ("_RPC_ChangeScene", RPCMode.All, scene);
	}
	[RPC]
	void _RPC_ChangeScene(string scene){
		Application.LoadLevel(scene);
	}

	public void setSeed(string seed){
		GetComponent<NetworkView> ().RPC ("_RPC_setSeed", RPCMode.All, seed);
	}
	[RPC]
	void _RPC_setSeed(string seed){
		MMPanelScript.usedSeed = seed;
		UnityEngine.Random.seed = seed.GetHashCode ();
	}
}



