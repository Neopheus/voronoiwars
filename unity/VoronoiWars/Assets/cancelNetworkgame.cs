﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class cancelNetworkgame : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void cancelJoining() {
		NetworkManager.instance.tryToConnect = false;
		Network.Disconnect();
		Debug.Log ("Cancel joining");
	}

	public void cancelHosting() {
		NetworkManager.instance.serverStarted = false;
		Network.Disconnect();
		Debug.Log ("Cancel hosting");
	}

	public void stopNetworking() {
		NetworkManager.instance.tryToConnect = false;
		NetworkManager.instance.serverStarted = false;
		Network.Disconnect();
		Debug.Log ("Stop networking");
	}
}
