﻿using UnityEngine;
using System.Collections;
using System;

/* 	
 * Author: Jakob Schaal
 */
public class TimeHelper {
	private static DateTime start;
	private static DateTime commit;

	public static void Start(){
		start = DateTime.Now;
		commit = DateTime.Now;
	}

	public static void Tic(string msg){
		DateTime now = DateTime.Now;
		TimeSpan time = now - commit;
		commit = now;

		Debug.Log (time.TotalMilliseconds + " " + msg);
	}

	public static void Stop(string msg){
		Tic (msg);
		Debug.Log ("Total Time: " + (DateTime.Now - start).TotalMilliseconds);
	}
}
