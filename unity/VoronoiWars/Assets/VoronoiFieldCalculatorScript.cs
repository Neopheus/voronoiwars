using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

/* 	
 * Author: Jakob Schaal
 */
public class VoronoiFieldCalculatorScript : MonoBehaviour {
	public static VoronoiFieldCalculatorScript instance;

	private bool iniDone = false;
	public List<VoronoiField> sideCenters;
	private List<VoronoiField>[,] sideCentersGrid;
	private const int SIDECENTERSGRIDAMOUNT = 16;
	private const double GRIDSIZE = size / SIDECENTERSGRIDAMOUNT;
	private List<VoronoiPosition> eventQueue;
	private List<Line2> voronoiEdges;
	private List<Line2> triangulation;
	private List<VoronoiField> spawnBlue;
	private List<VoronoiField> spawnRed;
	
	private List<VoronoiField>[] distancesToBlue = new List<VoronoiField>[32];
	private List<VoronoiField>[] distancesToRed  = new List<VoronoiField>[32];

	private GameObject hqRed;
	private GameObject hqBlue;

	private const int N = 200;

	public const double size = 16f;

	public GameObject canvas;
	public GameObject radialMenu;
	public GameObject PumpJackPrefab;
	public GameObject HQPrefab;

	private Terrain terrain;

	private Dictionary<Vector2Double, VoronoiField> vec2ToField = new Dictionary<Vector2Double, VoronoiField>();

	public GameObject highLight;
	
	private VoronoiField start;
	private List<VoronoiField> startNeighbors;
	private VoronoiField stop;

	private const int nearPump = 2;
	//private const int farPump = 4;

	// Use this for initialization
	void Start () {
		if (instance != null) {
			throw new UnityException("Singleton Violation!");
		}
		instance = this;


//		if (NetworkManager.instance.networkMode == NetworkManager.NetworkMode.hotSeat) {
//			calculateField(StartHotSeatScript.Seed);
//
//		}
		calculateField();
	}




	public void calculateField(){
		//UnityEngine.Random.seed = -42007916;
		print ("Seed: " + UnityEngine.Random.seed);
		terrain = this.GetComponent<Terrain> ();
		sideCenters = new List<VoronoiField> ();
		eventQueue = new List<VoronoiPosition> ();
		voronoiEdges = new List<Line2> ();
		triangulation = new List<Line2> ();
		spawnBlue = new List<VoronoiField> ();
		spawnRed = new List<VoronoiField> ();
		for (int i = 0; i<32; i++) {
			distancesToBlue[i] = new List<VoronoiField>();
			distancesToRed[i]  = new List<VoronoiField>();
		}
		for (int i = 0; i<N; i++) {
			VoronoiField vf = new VoronoiField (new Vector2Double (UnityEngine.Random.Range (0f, (float)size), UnityEngine.Random.Range (0f, (float)size)));
			sideCenters.Add (vf);
		}

		for (int i = 0; i<2; i++)
			spreadPoints ();
		/*sideCenters.Add (new VoronoiField(new Vector2 (1, 1)));
		sideCenters.Add (new VoronoiField(new Vector2 (2, 2)));
		sideCenters.Add (new VoronoiField(new Vector2 (3, 3)));
		sideCenters.Add (new VoronoiField(new Vector2 (4, 4)));
		sideCenters.Add (new VoronoiField(new Vector2 (1, 5)));*/
		sideCenters.Sort ();

		for (int i = 0; i<N; i++) {
			vec2ToField [sideCenters [i].pos] = sideCenters [i];
		}

		for (int i = 0; i<N; i++) {
			eventQueue.Add (sideCenters [i]);
			sideCenters[i].index = i;
		}

		sideCentersGrid = new List<VoronoiField>[SIDECENTERSGRIDAMOUNT, SIDECENTERSGRIDAMOUNT];
		for (int i = 0; i<SIDECENTERSGRIDAMOUNT; i++) {
			for(int k = 0; k<SIDECENTERSGRIDAMOUNT; k++){
				sideCentersGrid[i, k] = new List<VoronoiField>();
			}
		}

		ParabolicArc.root = new ParabolicArc (eventQueue [0].pos);
		eventQueue.RemoveAt (0);


		while (eventQueue.Count > 0) {
			VoronoiPosition vp = eventQueue [0];
			eventQueue.RemoveAt (0);

			int start = System.Environment.TickCount;

			if (vp is VoronoiCircleEvent) {
				VoronoiCircleEvent vce = (VoronoiCircleEvent)vp;
				handleCircleEvent (vce);
			} else if (vp is VoronoiField) {
				VoronoiField vf = (VoronoiField)vp;
				handleSiteEvent (vf);
			} else {
				throw new UnityException ("Illegal Type! Only VoronoiCircleEvent and VoronoiField are supported!");
			}
			//eventQueue.Sort ();
		}


		finish_edges ();



		for (int i = 0; i<triangulation.Count; i++) {
			try{
				VoronoiField vf1 = vec2ToField [triangulation [i].start];
				VoronoiField vf2 = vec2ToField [triangulation [i].end];
				vf1.addNeighbor (vf2);
			}
			catch(Exception e){
				Debug.LogWarning("Warning! " + i + "did not work at triangulation!");
			}

		}

		for (int i = 0; i<1000; i++) {
			double percentage = size * i / 1000;
			Vector2Double left = new Vector2Double (0, percentage);
			Vector2Double right = new Vector2Double (size, percentage);
			Vector2Double bottom = new Vector2Double (percentage, 0);
			Vector2Double top = new Vector2Double (percentage, size);

			VoronoiField topVF = closestSite (top);
			VoronoiField bottomVF = closestSite (bottom);

			closestSite (left)  .isBorderField = true;
			closestSite (right) .isBorderField = true;
			bottomVF            .isBorderField = true;
			topVF               .isBorderField = true;

			foreach (VoronoiField e in bottomVF.getNeighbors()) {
				if(spawnRed.Contains (e) == false){
					e.playerField = Factions.red;
					e.orginalSpawnField = true;
					e.distanceToRed = 0;
					spawnRed.Add (e);
					distancesToRed[0].Add (e);
				}
			}
			

			foreach (VoronoiField e in topVF.getNeighbors()) {
				if(spawnBlue.Contains (e) == false){
					e.playerField = Factions.blue;
					e.orginalSpawnField = true;
					e.distanceToBlue = 0;
					spawnBlue.Add (e);
					distancesToBlue[0].Add (e);
				}
			}
		}
		List<VoronoiField> orginalRed = new List<VoronoiField> (spawnRed);
		List<VoronoiField> orginalBlue = new List<VoronoiField> (spawnBlue);

		foreach (VoronoiField e in orginalRed) {
			if(e.isBorderField == false){
				foreach(VoronoiField ee in e.getNeighbors()){
					if(ee.isBorderField == false && spawnRed.Contains(ee) == false){
						ee.playerField = Factions.red;
						ee.distanceToRed = 0;
						spawnRed.Add (ee);
						distancesToRed[0].Add (ee);
					}
				}
			}
		}
		foreach (VoronoiField e in orginalBlue) {
			if(e.isBorderField == false){
				foreach(VoronoiField ee in e.getNeighbors()){
					if(ee.isBorderField == false && spawnBlue.Contains(ee) == false){
						ee.playerField = Factions.blue;
						ee.distanceToBlue = 0;
						spawnBlue.Add (ee);
						distancesToBlue[0].Add (ee);
					}
				}
			}
		}
		for (int i = 1; i<32; i++) {
			foreach(VoronoiField e in distancesToRed[i-1]){
				foreach(VoronoiField ee in e.getNeighbors()){
					if(ee.distanceToRed < 0 && ee.isBorderField == false){
						ee.distanceToRed = i;
						distancesToRed[i].Add (ee);
					}
				}
			}
		}
		for (int i = 1; i<32; i++) {
			foreach(VoronoiField e in distancesToBlue[i-1]){
				foreach(VoronoiField ee in e.getNeighbors()){
					if(ee.distanceToBlue < 0 && ee.isBorderField == false){
						ee.distanceToBlue = i;
						distancesToBlue[i].Add (ee);
					}
				}
			}
		}
		for (int i = 0; i<spawnRed.Count; i++) {
			if(spawnRed[i].isBorderField){
				spawnRed.RemoveAt(i);
				i--;
			}
		}
		for (int i = 0; i<spawnBlue.Count; i++) {
			if(spawnBlue[i].isBorderField){
				spawnBlue.RemoveAt (i);
				i--;
			}
		}
		placePumpJack(distancesToRed[nearPump]);
		//placePumpJack(distancesToRed[farPump]);
		placePumpJack(distancesToBlue[nearPump]);
		//placePumpJack(distancesToBlue[farPump]);
		//placePumpJack(getRandomEmptyNonPlayerField ());
		for (int i = 0; i<3; i++) {
			placePumpJackEqualDistance();
		}


		//placeHQ (spawnRed[UnityEngine.Random.Range (0, spawnRed.Count)]);

		for (int i = 0; i<N; i++) {
			VoronoiField vfLocal = sideCenters[i];
			int posX = getGridX (vfLocal);
			int posY = getGridY (vfLocal);
			sideCentersGrid[posX, posY].Add(vfLocal);
		}

		applyToTerrain ();


		GameObject haloParent = new GameObject ();
		haloParent.name = "Halo Parent";
		for (int i = 0; i<N; i++) {
			Vector3 pos = sideCenters [i].worldPos;
			sideCenters[i].light = (GameObject)GameObject.Instantiate(highLight, pos, Quaternion.Euler(new Vector3(90, 0, 0)));
			sideCenters[i].light.transform.parent = haloParent.transform;
		}

		turnOffAllLights ();

		triangulation = null; //give it to the GC

		iniDone = true;

		VoronoiField vfHqRed = null;
		VoronoiField vfHqBlue = null;

		for (int i = 0; i<128; i++) {
			vfHqRed = spawnRed[UnityEngine.Random.Range (0, spawnRed.Count)];
			if(vfHqRed.orginalSpawnField == false){
				break;
			}
			vfHqRed = null;
		}
		for (int i = 0; i<128; i++) {
			vfHqBlue = spawnBlue[UnityEngine.Random.Range (0, spawnBlue.Count)];
			if(vfHqBlue.orginalSpawnField == false){
				break;
			}
			vfHqBlue = null;
		}

		if (vfHqRed == null) {
			vfHqRed = spawnRed [UnityEngine.Random.Range (0, spawnRed.Count)];
		}
		if (vfHqBlue == null) {
			vfHqBlue = spawnBlue[UnityEngine.Random.Range (0, spawnBlue.Count)];
		}
		
		hqRed = placeHQ (vfHqRed, Factions.red);
		hqBlue = placeHQ (vfHqBlue, Factions.blue);

		for (int i = 0; i<N; i++) {
			sideCenters[i].id = i;
		}
	}


	public GameObject getHq(Factions faction){
		switch (faction) {
		case Factions.blue:
			return hqBlue;
		case Factions.red:
			return hqRed;
		default:
			throw new UnityException("Non supported Faction!");
		}
	}

	private int getGridX(VoronoiField vf){
		int pos = (int)(vf.pos.x / GRIDSIZE);
		return pos;
	}

	private int getGridY(VoronoiField vf){
		int pos = (int)(vf.pos.y / GRIDSIZE);
		return pos;
	}

	private int getGrid(double xy){
		int pos = (int)(xy / GRIDSIZE);
		/*if (pos < 0)
			pos = 0;
		if (pos >= SIDECENTERSGRIDAMOUNT)
			pos = SIDECENTERSGRIDAMOUNT - 1;*/
		return pos;
	}

	public void turnOffAllLights(){
		for (int i = 0; i<N; i++) {
			sideCenters[i].light.SetActive(false);
		}
	}

	public void resetAllHighlightColors(){
		for (int i = 0; i<N; i++) {
			try{
				sideCenters[i].light.GetComponent<SpriteRenderer>().color = Color.white;
			}catch(Exception e){
				Debug.LogError("Failed at " + i);
				throw e;
			}
		}
	}

	public void highlightHover(){
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		RaycastHit hit;
		if (Physics.Raycast (ray, out hit)) {
			VoronoiField target = sideCenters [closestSiteIndexWorldCoordinated (hit.point)];
			target.light.GetComponent<SpriteRenderer>().color = Color.red;

			if(Unit.selected != null && target != null){
				List<VoronoiField> path = Pathfinding.calculatePath(sideCenters, Unit.selected.vf, target);
				for(int i = 1; i<path.Count-1; i++){
					Color lightRed = new Color(1, 0.5f, 0.5f, 1);
					path[i].light.GetComponent<SpriteRenderer>().color = lightRed;
				}
			}
		}
	}

	public void highlightSelected(){
		if (Unit.selected != null) {
			Unit.selected.vf.light.GetComponent<SpriteRenderer>().color = Color.green;
		}
	}

	private void applyToTerrain(){
		TerrainData td = terrain.terrainData;
		ValueNoise vn = new ValueNoise (td.heightmapHeight, td.heightmapWidth);
		float[,] heights = td.GetHeights (0, 0, td.heightmapHeight, td.heightmapWidth);
		double smallest = float.MaxValue;
		double biggest = float.MinValue;
		for (int i = 0; i<N; i++) {
			if(sideCenters[i].isBorderField){
				sideCenters[i].height = 0;
			}else{
				sideCenters[i].height = vn.getValue((int)(sideCenters[i].pos.x / size * td.heightmapHeight), (int)(sideCenters[i].pos.y / size * td.heightmapWidth));
				if(sideCenters[i].height > biggest){
					biggest = sideCenters[i].height;
				}
				if(sideCenters[i].height < smallest){
					smallest = sideCenters[i].height;
				}
			}
		}
		double heightDiff = biggest - smallest;
		for (int i = 0; i<td.heightmapHeight; i++) {
			for(int k = 0; k<td.heightmapWidth; k++){
				int index = closestSiteIndexGrid (new Vector2Double(size * i /td.heightmapHeight, size * k / td.heightmapWidth));
				//heights[i,k] = (float)sideCenters[index].height;
				//heights[i,k] = vn.getValue((int)(i), (int)(k));
				heights[i,k] = ((float)sideCenters[index].height + vn.getValue((int)(i), (int)(k))) / 2f;
				if(sideCenters[index].height == 0) heights[i,k] = 0;
			}
		}
		td.SetHeights (0, 0, heights);

		float[,,] splatmap = new float[td.alphamapWidth, td.alphamapHeight, td.alphamapLayers];
		int[] textures = new int[N];
		for (int i = 0; i<N; i++) {
			if(sideCenters[i].playerField == Factions.none || sideCenters[i].isBorderField){
				double normalizedHeight = sideCenters[i].height - smallest;
				double rangePerTexture = heightDiff / (td.alphamapLayers - 4);
				//textures[i] = UnityEngine.Random.Range (0, td.alphamapLayers - 3);
				textures[i] = (int)(normalizedHeight/rangePerTexture);
			}else{
				if(sideCenters[i].playerField == Factions.blue){
					textures[i] = td.alphamapLayers - 2;
				}else{
					textures[i] = td.alphamapLayers - 1;
				}
			}
			if(sideCenters[i].isBorderField){
				textures[i] = td.alphamapLayers - 3;
			}
		}
		for(int i = 0; i<td.alphamapHeight; i++){
			for(int k = 0; k<td.alphamapWidth; k++){
				int index = closestSiteIndexGrid (new Vector2Double(size*i/td.alphamapHeight, size*k/td.alphamapWidth));
				splatmap[i, k, textures[index]] = 1;
			}
		}

		for (int i = 0; i<voronoiEdges.Count; i++) {
			Line2 edge = voronoiEdges[i];
			Vector2Double start = edge.start;
			Vector2Double dir = edge.dir;
			for(float k = 0; k<100; k++){
				Vector2Double linePos = start + dir*k/100;
				linePos /= size;
				int x = Mathf.Clamp((int)(linePos.x * td.alphamapWidth), 4, td.alphamapWidth-5);
				int y = Mathf.Clamp((int)(linePos.y * td.alphamapHeight), 4, td.alphamapHeight-5);

				//print (x + " " + y + " " + (td.alphamapLayers - 3));

				for(int j = 0; j<td.alphamapLayers; j++){
					splatmap[x, y, j] = 0;
					for(int z = 1; z<=4; z++){
						splatmap[x-z, y, j] = 0;
						splatmap[x+z, y, j] = 0;
						splatmap[x, y-z, j] = 0;
						splatmap[x, y+z, j] = 0;
					}
				}

				for(int j = 0; j<td.alphamapLayers; j++){
					splatmap[x, y, td.alphamapLayers - 3] = 1;
					for(int z = 1; z<=4; z++){
						splatmap[x-z, y, td.alphamapLayers - 3] = 1;
						splatmap[x+z, y, td.alphamapLayers - 3] = 1;
						splatmap[x, y-z, td.alphamapLayers - 3] = 1;
						splatmap[x, y+z, td.alphamapLayers - 3] = 1;
					}
				}
			}
		}

		td.SetAlphamaps (0, 0, splatmap);

	}

	private void placePumpJack(List<VoronoiField> vf){
		print (vf.Count);
		VoronoiField vfLocal = null;
		do{
			vfLocal = vf[UnityEngine.Random.Range (0, vf.Count)];
		}while(vf.Count > 4 && vfLocal.pumpJack != null);


		GameObject go = (GameObject)GameObject.Instantiate (PumpJackPrefab);
		go.GetComponent<PumpJackScript> ().field = vfLocal;
		vfLocal.pumpJack = go;
	}

	private void placePumpJack(VoronoiField vf){
		if (vf.pumpJack != null) {
			throw new UnityException("");
		}

		GameObject go = (GameObject)GameObject.Instantiate (PumpJackPrefab);
		go.GetComponent<PumpJackScript> ().field = vf;
		vf.pumpJack = go;
	}

	public void placePumpJackEqualDistance(){
		List<VoronoiField> equalDist = new List<VoronoiField> ();
		for (int i = 0; i<N; i++) {
			VoronoiField vf = sideCenters[i];
			if(vf.isBorderField == false && vf.distanceToBlue != -1 && vf.distanceToRed != -1 && vf.distanceToBlue == vf.distanceToRed && vf.pumpJack == null){
				equalDist.Add (vf);
			}
		}

		if (equalDist.Count != 0) {
			placePumpJack (equalDist[UnityEngine.Random.Range (0, equalDist.Count)]);
		}
	}

	private GameObject placeHQ(VoronoiField vf, Factions faction){
		GameObject go = (GameObject)GameObject.Instantiate(HQPrefab, vf.worldPos, Quaternion.identity);
		Unit unit = go.GetComponent<Unit> ();
		unit.ini (UnitType.hq, vf);
		unit.faction = faction;
		return go;
	}


	private void spreadPoints(){
		int gridAcuteness = 100;
		double[] xs = new double[N];
		double[] ys = new double[N];
		double[] amounts = new double[N];
		for (int i = 0; i<gridAcuteness; i++) {
			for(int k = 0; k<gridAcuteness; k++){
				Vector2Double pos = new Vector2Double(size * i / gridAcuteness, size*k/gridAcuteness);
				int index = closestSiteIndex(pos);
				xs[index] += pos.x;
				ys[index] += pos.y;
				amounts[index]++;
			}
		}

		for (int i = 0; i<N; i++) {
			sideCenters[i].pos = new Vector2Double((double)(xs[i]/amounts[i]), (float)(ys[i]/amounts[i]));
		}
	}

	private int closestSiteIndexGrid(Vector2Double pos){
		int ret = -1;
		double closestDist = double.MaxValue;
		int posX = getGrid (pos.x);
		int posY = getGrid (pos.y);
		


		int minX = Mathf.Max (posX - 1, 0);
		int minY = Mathf.Max (posY - 1, 0);
		int maxX = Mathf.Min (posX + 1, SIDECENTERSGRIDAMOUNT - 1);
		int maxY = Mathf.Min (posY + 1, SIDECENTERSGRIDAMOUNT - 1);
		
		for (int i = minX; i<= maxX; i++) {
			for (int k = minY; k<= maxY; k++) {
				List<VoronoiField> fieldsLocal = sideCentersGrid[i,k];
				for(int m = 0; m<fieldsLocal.Count; m++){
					double dist = Vector2Double.DistanceSq(pos, fieldsLocal[m].pos);
					if(dist < closestDist){
						closestDist = dist;
						ret = fieldsLocal[m].index;
					}
				}
			}
		}
		
		/*for (int i = 0; i<N; i++) {
			double dist = Vector2Double.DistanceSq(pos, sideCenters[i].pos);
			if(dist < closestDist){
				closestDist = dist;
				ret = i;
			}
		}*/
		return ret;
	}

	private int closestSiteIndex(Vector2Double pos){
		int ret = -1;
		double closestDist = double.MaxValue;
		
		for (int i = 0; i<N; i++) {
			double dist = Vector2Double.DistanceSq(pos, sideCenters[i].pos);
			if(dist < closestDist){
				closestDist = dist;
				ret = i;
			}
		}
		return ret;
	}

	private VoronoiField closestSite(Vector2Double pos){
		return sideCenters [closestSiteIndex (pos)];
	}

	private int closestSiteIndexWorldCoordinated(Vector3 pos){
		int ret = -1;
		double closestDist = double.MaxValue;
		for (int i = 0; i<N; i++) {
			double dist = Vector3.Distance(pos, sideCenters[i].worldPos);
			if(dist < closestDist){
				closestDist = dist;
				ret = i;
			}
		}
		return ret;
	}


	private void handleCircleEvent(VoronoiCircleEvent vce){
		if (!vce.valid)
			return;
		vce.valid = false;

		Line2 line = new Line2 (vce.pos, voronoiEdges);
		ParabolicArc pa = vce.a;

		if (pa.previous != null) {
			triangulation.Add (new Line2(pa.spawnPoint, pa.previous.spawnPoint));
			pa.previous.next = pa.next;
			pa.previous.l2 = line;
		}
		if (pa.next != null) {
			triangulation.Add (new Line2(pa.spawnPoint, pa.next.spawnPoint));
			pa.next.previous = pa.previous;
			pa.next.l1 = line;
		}

		if (pa.l1 != null) {
			pa.l1.finish(vce.pos);
		}
		if (pa.l2 != null) {
			pa.l2.finish(vce.pos);
		}

		if (pa.previous != null) {
			createNewCircleEvent(pa.previous, vce.triggerPosition);
		}
		if (pa.next != null) {
			createNewCircleEvent(pa.next, vce.triggerPosition);
		}
	}




	private void handleSiteEvent(VoronoiField vp){
		ParabolicArc currentArc = ParabolicArc.root;
		for (; currentArc != null; currentArc = currentArc.next) {
			if(currentArc.wouldNewParabolaIntersect(vp.pos)){
				Vector2Double intersectionPoint = currentArc.intersect(vp.pos);
				if(currentArc.next != null && !currentArc.next.wouldNewParabolaIntersect(vp.pos)){
					//noch 'ne Kopie einfügen für rechts und links gleich
					currentArc.insert (new ParabolicArc(currentArc.spawnPoint));
				}
				else{
					currentArc.next = new ParabolicArc(currentArc.spawnPoint, currentArc);
				}
				currentArc.next.l2 = currentArc.l2;

				ParabolicArc newArc = new ParabolicArc(vp.pos);

				//Und die neue Arc dazwischen einfügen
				currentArc.insert (newArc);
				
				newArc.l1 = new Line2(intersectionPoint, voronoiEdges);
				newArc.previous.l2 = newArc.l1;
				triangulation.Add (new Line2(newArc.spawnPoint, newArc.previous.spawnPoint));
				newArc.l2 = new Line2(intersectionPoint, voronoiEdges);
				newArc.next.l1 = newArc.l2;
				triangulation.Add (new Line2(newArc.spawnPoint, newArc.next.spawnPoint));

				createNewCircleEvent (newArc.previous, vp.pos.x);
				createNewCircleEvent (newArc, vp.pos.x);
				createNewCircleEvent (newArc.next, vp.pos.x);

				return;
			}
		}


		for (currentArc = ParabolicArc.root; currentArc.next != null; currentArc = currentArc.next);

		currentArc.next = new ParabolicArc (vp.pos, currentArc);
		currentArc.next.l1 = new Line2 (new Vector2Double (0, (currentArc.next.spawnPoint.y + currentArc.spawnPoint.y) / 2f), voronoiEdges);
		triangulation.Add (new Line2(currentArc.spawnPoint, currentArc.next.spawnPoint));
		currentArc.l2 = currentArc.next.l1;
	}

	private void createNewCircleEvent(ParabolicArc pa, double sweepLine){
		if (pa.e != null && pa.e.pos.x != sweepLine) {
			pa.e.valid = false;
		}
		pa.e = null;

		//damit es einen neuen circle event geben kann müssen next und previous belegt sein
		if (pa.previous == null || pa.next == null) {
			return;
		}

		double rightOfCircle = 0;
		try{
			Vector2Double a = pa.previous.spawnPoint;
			Vector2Double b = pa.spawnPoint;
			Vector2Double c = pa.next.spawnPoint;
			if((b.x-a.x)*(c.y-a.y) - (c.x-a.x)*(b.y-a.y) > 0) return;
			Circle2 cir = new Circle2 (a, b, c);
			rightOfCircle = cir.center.x + cir.radius;
			if(rightOfCircle > sweepLine){
				pa.e = new VoronoiCircleEvent(rightOfCircle, cir.center, pa);
				eventQueue.Add (pa.e);
				eventQueue.Sort ();
			}
		}catch(UnityException e){
			//Hier kann eine Exception fliegen. Zum Beispiel wenn die 3 punkte colinear sind. Dann wird einfach kein neues Event erzeugt.
			//Ein weiteres Exceptionhandling ist nicht nötig.
		}
	}

	private void finish_edges(){
		double sweepLine = size * 6;
		for (ParabolicArc arc = ParabolicArc.root; arc.next != null; arc = arc.next) {
			if(arc.l2 != null && arc.l2.completlyInitialized == false){
				arc.calculateABC(sweepLine);
				arc.l2.finish(arc.intersect(arc.next, sweepLine));
			}
		}
	}

	// Update is called once per frame
	void Update () {
		if (Debug.isDebugBuild) {
			for (int i = 0; i<sideCenters.Count; i++) {
				if (sideCenters [i].unit != null) {
					if (sideCenters [i].unit.vf != sideCenters [i]) {
						throw new UnityException ("Illegal Unit-VF State!");
					}
				}
			}
		}
	
		resetAllHighlightColors ();
		highlightHover ();
		highlightSelected ();
		
		VoronoiField clickedField = null;
		if (Input.GetMouseButtonDown (0)) {
			Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
			RaycastHit hit;
			if (Physics.Raycast (ray, out hit) && !UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject ()) {
				//Click (hit.point);

				if (GameSupervisor.instance.selectedField == null) {
					clickedField = sideCenters [closestSiteIndexWorldCoordinated (hit.point)];
					GameSupervisor.instance.selectedField = clickedField;
					if (GameSupervisor.instance.selectedField.unit == null 
						&& GameSupervisor.instance.selectedField.playerField == GameSupervisor.instance.faction
						&& GameSupervisor.instance.selectedField.isBorderField == false) {
						Vector3 radialPos = Camera.main.WorldToViewportPoint (GameSupervisor.instance.selectedField.worldPos);

						radialPos.x -= 0.5f;
						radialPos.y -= 0.5f;

						radialPos.x *= canvas.GetComponent<RectTransform> ().rect.width;
						radialPos.y *= canvas.GetComponent<RectTransform> ().rect.height;
						radialPos.z = 0;
					
						radialMenu.GetComponent<RadialScript> ().activateRadialMenu ();
						radialMenu.transform.localPosition = radialPos;
					}


					if (GameSupervisor.instance.selectedField.isBorderField == true || GameSupervisor.instance.selectedField.unit != null) {
						GameSupervisor.instance.selectedField = null;
					}
				}
			}
		}


		if (clickedField != null) {
			if (Unit.selected != null && Unit.selected.state == UnitState.ready) {
				if (clickedField.unit == null) {
					if (Unit.selected.neighbors.Contains (clickedField)) {
						Unit.selected.GetComponent<AudioSource>().PlayOneShot(Unit.selected.moveSound);
						if(NetworkManager.instance.networkMode == NetworkManager.NetworkMode.hotSeat){
							Unit.selected.MoveTo (clickedField);
						} else {
							Unit.selected.MoveToRPC (Unit.selected.vf.id, clickedField.id, true);
						}
						GameSupervisor.instance.selectedField = null;
					} else if (GameSupervisor.instance.selectedField != null) {
						Unit.selected = null;
						turnOffAllLights ();
					}
				} else {
					if (Unit.selected.neighbors.Contains (clickedField) && Unit.selected.faction != clickedField.unit.faction && clickedField.unit.state != UnitState.fighting && clickedField.unit.state != UnitState.dying) {
						Unit.selected.GetComponent<AudioSource>().PlayOneShot(Unit.selected.attackSound);
						if(NetworkManager.instance.networkMode == NetworkManager.NetworkMode.hotSeat){
							Unit.selected.Attack (clickedField);
						} else {
							Unit.selected.AttackRPC (Unit.selected.vf.id, clickedField.id, true);
						}
						print ("peng");
					} else if (GameSupervisor.instance.selectedField != null) {
						Unit.selected = null;
						turnOffAllLights ();
					}
				}
			}
		}
	}


	void OnDrawGizmos(){
		if (!iniDone)
			return;


		/*Gizmos.color = new Color(UnityEngine.Random.Range (0f, 1f), UnityEngine.Random.Range (0f, 1f), UnityEngine.Random.Range (0f, 1f), 1);
		for (int i = 0; i<N; i++) {
			Gizmos.DrawSphere(sideCenters[i].pos, 0.1f);
		}

		Gizmos.color = Color.white;
		for(int i = 0; i<voronoiEdges.Count; i++){
			if(voronoiEdges[i].completlyInitialized) Gizmos.DrawLine(voronoiEdges[i].start, voronoiEdges[i].end);
		}

		Gizmos.color = Color.red;
		for(int i = 0; i<triangulation.Count; i++){
			if(triangulation[i].completlyInitialized) Gizmos.DrawLine(triangulation[i].start, triangulation[i].end);
		}

		Gizmos.color = Color.red;
		for(int i = 0; i<N; i++){
			List<VoronoiField> list = sideCenters[i].getNeighbors();
			for(int k = 0; k<list.Count; k++){
				Vector3 pos1 = sideCenters[i].worldPos;
				Vector3 pos2 = list[k].worldPos;

				Gizmos.DrawLine (pos1, pos2);
			}
		}*/
	}

	//WARNING! May return null!
	public VoronoiField getRandomEmptyNonPlayerField(){
		VoronoiField vf = null;
		for (int i = 0; i<128; i++) {
			vf = sideCenters[UnityEngine.Random.Range (0, sideCenters.Count)];
			if(vf.unit == null && vf.isBorderField == false && vf.playerField == Factions.none && vf.pumpJack == null && vf.SupplyCrate == null){
				break;
			}
			vf = null;
		}
		return vf;
	}

	public VoronoiField getFieldById(int id){
		return sideCenters [id];
	}

	public List<VoronoiField> Click(VoronoiField vf){  //get neighbors

		turnOffAllLights ();
		
		start = vf;
		List<VoronoiField> neighbors = RangeDetection.calculateRange (vf, 3);
		startNeighbors = neighbors;
		
		for (int i = 0; i<neighbors.Count; i++) {
			neighbors[i].light.SetActive(true);
		}

		return neighbors;
	}
	
	public void RightClick(Vector3 pos){
		int handle = closestSiteIndexWorldCoordinated (pos);
		turnOffAllLights ();
		stop = sideCenters [handle];
		if (start != null) {
			List<VoronoiField> path = Pathfinding.calculatePath(startNeighbors, start, stop);
			for(int i = 0; i<path.Count; i++){
				path[i].light.SetActive(true);
			}
		}
	}

	public void LookAtHq(GameObject ob, Factions faction){
		GameObject hq = getHq (faction);
		if (hq != null) {
			Vector3 lookAt = hq.transform.position;
			lookAt.y = ob.transform.position.y;
			ob.transform.LookAt (lookAt);
		}
	}
}
