﻿using UnityEngine;
using System.Collections;

/**
 * Author: Jakob Schaal
 */
public class PulseWhenDone : MonoBehaviour {

	private Vector3 startScale;
	private float animationTime = 0;

	// Use this for initialization
	void Start () {
		startScale = this.transform.localScale;
	}
	
	// Update is called once per frame
	void Update () {
		if (isDone ()) {
			animationTime += Time.deltaTime * 2.5f;
			if(GameSupervisor.instance.resourceCurrent < 100){
				animationTime += Time.deltaTime * 7.5f;
			}
			this.transform.localScale = startScale * ((-Mathf.Cos (animationTime) * 0.1f) + 1.1f);
		} else {
			this.transform.localScale = startScale;
		}
	}

	private bool isDone(){
		string search = "UnitRed";
		if (GameSupervisor.instance.faction == Factions.blue) {
			search = "UnitBlue";
		}
		GameObject[] objects = GameObject.FindGameObjectsWithTag (search);
		bool foundReady = false;
		for (int i = 0; i<objects.Length; i++) {
			if(objects[i].GetComponent<Unit>().state == UnitState.ready){
				foundReady = true;
				break;
			}
		}

		return !foundReady;
	}
}
