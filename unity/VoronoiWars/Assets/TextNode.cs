﻿using UnityEngine;
using System.Collections;

/**
 * Author: Jakob Schaal
 */
public class TextNode : MonoBehaviour {
	private float timeAlive = 0;
	private Vector3 startPos;
	private TextMesh textMesh;

	// Use this for initialization
	void Start () {
		startPos = this.transform.position;
		textMesh = this.transform.GetChild(0).GetComponent<TextMesh> ();
		Update ();
	}
	
	// Update is called once per frame
	void Update () {
		timeAlive += Time.deltaTime / 5;

		textMesh.color = new Color(1, 1, 1, Interpolations.interpolLinear(1, 0, timeAlive));
		this.transform.position = startPos + new Vector3 (0, Interpolations.interpolLinear(0, 10, timeAlive), 0);


		this.transform.LookAt (Camera.main.transform.position);
		Quaternion rot = this.transform.rotation;
		this.transform.rotation = rot;

		if (timeAlive > 1) {
			GameObject.Destroy (this.gameObject);
		}
	}
}
